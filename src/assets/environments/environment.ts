export const environment = {
  apiUrl: 'https://7finmcpazj.execute-api.us-east-1.amazonaws.com/dev/sv_dev_sccaapi_v1',
  auditTypes: ['Insert', 'Delte', 'Update'],
  sqlLanguageTypes :[{label:'PG Sql',value:'pgsql'},{label:'Microsoft SQL',value:'mssql'},{label:'Oracle',value:'oracle'}],
  cognito: {
    userPoolId: 'us-east-1_eqXEco5DX', 
    userPoolWebClientId: '49sniu2t8ci82etudn5atodg5n' 
  },
  backgrounderLoader:['testDBConnection']
};
