import { PrimeNGConfig } from 'primeng/api';
import { Component,ElementRef,OnInit, Renderer2, ViewChild,OnDestroy, NgZone  } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { DropdownFilterOptions } from 'primeng/dropdown';
@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.scss']
})
export class PermissionComponent {
  @ViewChild('fullscreenDiv') fullscreenDiv: ElementRef | undefined;
  @ViewChild('fullScreen',{read:ElementRef}) divRef!: ElementRef<any>;
 @ViewChild('openUsers',{read: ElementRef})closeusersRef!:ElementRef<any>;
 @ViewChild('closeUsers',{read: ElementRef})modelusersRef!:ElementRef<any>;
 @ViewChild('openGroups',{read: ElementRef})closegroupsRef!:ElementRef<any>;
 @ViewChild('closeGroups',{read: ElementRef})modelgroupsRef!:ElementRef<any>;
  isButtonRounded: boolean = false;
  activeIndex: number = 0;

  products:any=[{
    id: '1000',
    code: 'f230fh0g3',
    name: 'Bamboo Watch',
    description: 'Product Description',
    image: 'bamboo-watch.jpg',
    price: 65,
    category: 'Accessories',
    quantity: 24,
    inventoryStatus: 'INSTOCK',
    rating: 5
},{
  id: '1000',
  code: 'f230fh0g3',
  name: 'Bamboo Watch',
  description: 'Product Description',
  image: 'bamboo-watch.jpg',
  price: 65,
  category: 'Accessories',
  quantity: 24,
  inventoryStatus: 'INSTOCK',
  rating: 5
},{
  id: '1000',
  code: 'f230fh0g3',
  name: 'Bamboo Watch',
  description: 'Product Description',
  image: 'bamboo-watch.jpg',
  price: 65,
  category: 'Accessories',
  quantity: 24,
  inventoryStatus: 'INSTOCK',
  rating: 5
},{
  id: '1000',
  code: 'f230fh0g3',
  name: 'Bamboo Watch',
  description: 'Product Description',
  image: 'bamboo-watch.jpg',
  price: 65,
  category: 'Accessories',
  quantity: 24,
  inventoryStatus: 'INSTOCK',
  rating: 5
}]

selectedProducts = this.products;

  files: File[] = [];

  totalSize: number = 0;

  totalSizePercent: number = 0;

  items = ['Pradeep', 'Mohan', 'Arun', 'Sarath', 'Divyaprasad'];

  constructor(private config: PrimeNGConfig) {}

  visible: boolean = false;

  showDialog() {
    this.visible = true;
  }

  choose(event: any, callback: any) {
    callback();
  }

  onRemoveTemplatingFile(
    event: any,
    file: any,
    removeFileCallback: any,
    index: any
  ) {
    removeFileCallback(event, index);
    this.totalSize -= parseInt(this.formatSize(file.size));
    this.totalSizePercent = this.totalSize / 10;
  }

  onClearTemplatingUpload(clear: any) {
    clear();
    this.totalSize = 0;
    this.totalSizePercent = 0;
  }

  onTemplatedUpload() {
    // this.messageService.add({
    //   severity: 'info',
    //   summary: 'Success',
    //   detail: 'File Uploaded',
    //   life: 3000,
    // });
  }

  onSelectedFiles(event: any) {
    this.files = event.currentFiles as File[];
    this.files.forEach((file) => {
      this.totalSize += parseInt(this.formatSize(file.size));
    });
    this.totalSizePercent = this.totalSize / 10;
  }

  uploadEvent(callback: any) {
    callback();
  }

  formatSize(bytes: any) {
    const k = 1024;
    const dm = 3;
    const sizes = this.config.translation.fileSizeTypes || ['Bytes'];
    if (bytes === 0) {
      return `0 ${sizes[0]}`;
    }

    const i = Math.floor(Math.log(bytes) / Math.log(k));
    const formattedSize = parseFloat((bytes / Math.pow(k, i)).toFixed(dm));

    return `${formattedSize} ${sizes[i]}`;
  }

  tabview(event: any) {
    console.log(event);
    if (event === 1) {
      this.isButtonRounded = true;
    } else {
      this.isButtonRounded = false;
    }
  }
  openuserModal(){
this.closeusersRef.nativeElement.click();
  }
  closeuserModal(){
    this.modelusersRef.nativeElement.click();
  }
  openGroupsModal(){
    this.closegroupsRef.nativeElement.click();
  }
  closeGroupsModal(){
    this.modelgroupsRef.nativeElement.click();
  }
}
