// Default import
import { Component, ElementRef, OnInit, Renderer2, ViewChild, OnDestroy, NgZone, TemplateRef } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { ApiService } from '../services/api.service';
import { ToastrService } from 'ngx-toastr';
import { MessageService } from 'primeng/api';
import { MenuItem } from 'primeng/api';
import { StepsModule } from 'primeng/steps';
import { Subscription } from 'rxjs';
// Shared service import
import { TicketstepperService } from '../services/ticketstepper.service';
import { SharedService } from '../services/shared.service';
import { FormBuilder, Validators } from '@angular/forms';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Data } from '@angular/router';
import { DropdownFilterOptions } from 'primeng/dropdown';
import { faUnderline } from '@fortawesome/free-solid-svg-icons';
import { environment } from 'src/assets/environments/environment';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { TabsetComponent } from 'ngx-bootstrap/tabs';


interface DataSourceCard {
  [x: string]: any;
  DataSourceName: string;
  DataSoureType: string;
  Databasename: string;
  hostname: string;
  dateString: string;
  // Add any other properties you need
}
interface country {
  name?: string;
  code?: string;
}
interface ITab {
  title: string;
  content: string;
  removable: boolean;
  disabled: boolean;
  active?: boolean;
  customClass?: string;
}


@Component({
  selector: 'app-scheduler',
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.scss'],
})
export class SchedulerComponent {

   //Children declarations
   @ViewChild('open', { read: ElementRef }) closeRef!: ElementRef<any>;
   @ViewChild('close', { read: ElementRef }) modelRef!: ElementRef<any>;
   @ViewChild('staticTabs', { static: false }) staticTabs?: TabsetComponent;
 
   // String type variables
   searchKey: string = '';
   checked: boolean = false;
   sqlTypeSearchValue: string | undefined = '';
   sqlTypeValue = 'pgsql';
   datasourcePriTestResult = 'none';
   datasourceSecTestResult = 'none';
   tabsOverviewShow:boolean=false;
   //Integer variables
   currentStep = 1;
   numSteps = 3;
   index = 0;
   active = 0;
 
   //object variables
   dataSourceModel = {
     "action": "addDataSource",
     "id": 0,
     "name": "",
     "type": "",
     "primaryHost": "",
     "primaryPort": "",
     "primaryDatabaseName": "",
     "primaryUsername": "",
     "primaryPassword": "",
     "secondaryHost": "",
     "secondaryPort": "",
     "secondaryDatabaseName": "",
     "secondaryUsername": "",
     "secondaryPassword": "",
     "isEnable": true,
     "timezone": "",
     "description": "",
     "failsafe": false,
     "status": "",
     "userid": 0
   }
 
   //Boolean variables
   isEdit: boolean = false;
   popupPrimarySelected: boolean = true;
   configPage: boolean = false;
   basicDetailspage: boolean = false;
   spinnerTriggered: boolean = false;
   basicFieldsValid = false;
   configFieldValid = false;
   showErrors = false;
 
   countries: country[] | undefined;

   selectedCountry: country | null = null;
   // List type variables
   dataSourceList: any[] = [];
   sqlLanguageTypes: any[] = environment.sqlLanguageTypes;
   formattedDate: any;
   dateString: string = "2024-03-10T 10:10:10"; // Note: Use 'T' instead of space for ISO 8601 compliance
   dateObject = new Date(this.dateString);
   borderprimaryActive: boolean = false;
   datasourceitems: any
   datasourceactiveItem: any | undefined;
   formGroup: FormGroup | any;
   dataSource: any;
   inputParameters:boolean=false;
 
   tabs: ITab[] = [
    { title: 'Servion Reports', content: 'Dynamic content 1', removable: false, disabled: false},
    { title: 'MOE Reports', content: 'Dynamic content 2', removable: false, disabled: false},
  ];

  categories: any[] = [
    { name: 'One Time Schedule', key: 'O' },
    { name: 'Recurring Scheduler', key: 'R' },
];
 
   items: MenuItem[] | undefined;
   oneTimescheduleEnabled:boolean = false;
   recurssivescheduleenabled:boolean = true;
 
   constructor(
     private _apiService: ApiService,
     private _toastr: ToastrService,
     public _sharedService: SharedService,
     public messageService: MessageService, private renderer: Renderer2, private elRef: ElementRef,
     public ticketService: TicketstepperService, private _formBuilder: FormBuilder,
     private modalService: BsModalService,
   ) { }
 
   firstFormGroup = this._formBuilder.group({
     firstCtrl: ['', Validators.required],
   });
   secondFormGroup = this._formBuilder.group({
     secondCtrl: ['', Validators.required],
   });
 
 
   ngOnInit() {
     this.clear();
   }
 
   onRadioChange(key: any) {
    console.log("key is:",key)
    if(key==='O'){
      this.oneTimescheduleEnabled = true;
      this.recurssivescheduleenabled = false;
    }else if(key==='R'){
      this.oneTimescheduleEnabled = false;
      this.recurssivescheduleenabled = true;
    }
   }
 
 
 
   filterTypes(event: any, options: any[]) {
 
     this.sqlLanguageTypes = environment.sqlLanguageTypes.filter(option =>
       option.label.toLowerCase().includes(this.sqlTypeSearchValue ?? '')
     );
   }
 
   nextStep(): void {
    this.currentStep++;
    console.log("current step is:",this.currentStep);
    if (this.currentStep > this.numSteps) {
      this.currentStep = 1;
    }
    const steps = this.elRef.nativeElement.querySelectorAll('.step');

    steps.forEach((step:any, index:any) => {
      const stepNum = index + 1;
      if (stepNum === this.currentStep) {
        this.renderer.addClass(step, 'editing');
      } else {
        this.renderer.removeClass(step, 'editing');
      }
      if (stepNum < this.currentStep) {
        this.renderer.addClass(step, 'done');
      } else {
        this.renderer.removeClass(step, 'done');
      }
    });

    if(this.currentStep === 2){
      this.basicDetailspage=false;
      this.configPage = true;
      this.inputParameters=false;
      this.oneTimescheduleEnabled = false;
      this.recurssivescheduleenabled = false;
    }else if(this.currentStep === 3){
      this.basicDetailspage=false;
      this.configPage = false;
      this.inputParameters=true;
    }else if(this.currentStep === 4){
      this.basicDetailspage=false;
      this.configPage = false;
      this.inputParameters=false;
    }else{
      this.basicDetailspage=true;
      this.configPage = false;
      this.inputParameters=false;
    }
    

  }

  backStep(step: any){
    if (this.currentStep > 1) {
      this.currentStep--;
    } else {
      this.currentStep = this.numSteps;
    }
    const steps = this.elRef.nativeElement.querySelectorAll('.step');
  
    steps.forEach((step:any, index:any) => {
      const stepNum = index + 1;
      if (stepNum === this.currentStep) {
        this.renderer.addClass(step, 'editing');
      } else {
        this.renderer.removeClass(step, 'editing');
      }
      if (stepNum < this.currentStep) {
        this.renderer.addClass(step, 'done');
      } else {
        this.renderer.removeClass(step, 'done');
      }
    });
    if(this.currentStep === 2){
      this.basicDetailspage=false;
      this.configPage = true;
      this.inputParameters=false;
    }else if(this.currentStep === 3){
      this.basicDetailspage=false;
      this.configPage = false;
      this.inputParameters=true;
    }else if(this.currentStep === 4){
      this.basicDetailspage=false;
      this.configPage = false;
      this.inputParameters=false;
    }else{
      this.basicDetailspage=true;
      this.configPage = false;
      this.inputParameters=false;
    }
    
  }
 
   // Method to reset/clear all fields.
   clear = () => {
     this.dataSource = Object.assign({}, this.dataSourceModel);
     this.dataSourceList = [];
     this.searchKey = '';
     this.sqlTypeSearchValue = '';
     this.sqlLanguageTypes = environment.sqlLanguageTypes;
     this.getAllDatasource();
   };
 
   // Method to call when search icon is pressed.
   search = () => {
     this.getAllDatasource();
   };
 
   // On change in sorting option.
   onSortChange = (field: string, order: string) => {
     this.dataSourceList = this._sharedService.sorting(
       this.dataSourceList,
       field,
       order
     );
   };
 
   addNewTab(): void {
    const newTabIndex = this.tabs.length + 1;
    this.tabs.push({
      title: `Dynamic Title ${newTabIndex}`,
      content: `Dynamic content ${newTabIndex}`,
      disabled: false,
      removable: true
    });
  }
 
  removeTabHandler(tab: ITab): void {
    this.tabs.splice(this.tabs.indexOf(tab), 1);
    console.log('Remove Tab handler');
  }


   selectTab(tabId: number) {
    if (this.staticTabs?.tabs[tabId]) {
      this.staticTabs.tabs[tabId].active = true;
    }
  }
 
 
   fieldValidation(stepper: string) {
     if (stepper === 'basic') {
       if (this.dataSource.name != '' && this.dataSource.type != '') {
         this.basicFieldsValid = true;
         return;
       }
       this.basicFieldsValid = false;
       return;
     }
     else if (stepper === 'config') {
       if (this.dataSource.primaryHost != '' && this.dataSource.primaryPort != '' && this.dataSource.primaryUsername != '' && this.dataSource.primaryPassword != '' && this.dataSource.primaryDatabaseName != ''
         && (!this.dataSource.failsafe || (this.dataSource.secondaryHost != '' && this.dataSource.secondaryPort != '' && this.dataSource.secondaryUsername != '' && this.dataSource.secondaryPassword != '' && this.dataSource.secondaryDatabaseName != ''))
       ) {
         this.configFieldValid = true;
         return;
       }
       this.configFieldValid = false;
       return;
     }
   }
 
   // Method to Get All Datasource from Database.
   getAllDatasource = () => {
     this._apiService
       .makeAPICall({
         action: 'getAllDataSource',
         searchKey: this.searchKey,
       })
       .subscribe((response) => {
         if (response && response.status) {
           if (response.data.length > 0) {
             this.dataSourceList = response.data;
           } else {
           }
         }
       });
   };

   overViewmanagement=()=>{
    console.log("am i clicking")
    this.tabsOverviewShow = true;
    console.log(this.tabsOverviewShow)
    if(this.tabsOverviewShow===true){
      setTimeout(()=>{
        this.selectTab(1)
      },500)
    }
  
  }
 
   // Insert Datasource if type is 'add' else updates the existing report builder.
   // Clone existing Datasource and create a new one
   storeDataSource = () => {
     this.showErrors = true;
     let allowSaveInBasicPage = false;
 
     //Condition - check whether the basic fields are valid or not in edit popup
     if (this.isEdit && this.basicDetailspage) {
       this.fieldValidation('basic');
       if (!this.basicFieldsValid) {
         this._toastr.info('Please Fill the required fields');
         return;
       }
       allowSaveInBasicPage = true;
     }
 
 
     //Condition - check whether the config fields are valid or not in add/edit popup
     if (!allowSaveInBasicPage) {
       this.fieldValidation('config');
       if (!this.configFieldValid) {
         this._toastr.info('Please Fill the required fields');
         return;
       }
       else {
         if (this.datasourcePriTestResult === 'none') {
           this._toastr.info('Please test the primary connection');
           return;
         }
         else if (this.dataSource.failsafe && this.datasourcePriTestResult === 'none') {
           this._toastr.info('please test the secondary connection');
           return;
         }
       }
     }
     //ADD - if both basic and config fields are validated , Store it
     //EDIT -if either basic or config fields are validdated, save it
     this._apiService
       .makeAPICall(this.dataSource)
       .subscribe((response) => {
 
         if (response.status && response.data.message.split('-')[1] === 'true') {
           this._toastr.success(('Datasource ' + (this.isEdit ? 'updated' : 'inserted' + ' successfully')));
           this.closeRef.nativeElement.click();
         }
         else {
           this._toastr.error((this.isEdit ? 'Update Error' : 'Insert Error'), response.data.message.split('-')[0], { disableTimeOut: true });
         }
         console.log('response', response);
       });
   };
 
   toggleDeleteModal(action: string, template: TemplateRef<any>) {
     if (action === 'open')
       this.modalService.show(template);
     else if (action === 'close')
       this.modalService.hide();
   }
 
   // Method to Delete Datasource from Database.
   deleteDatasource = () => {
     let obj = {};
     this._apiService.makeAPICall(obj).subscribe((response) => {
       // Dismiss the modal after successful deletion
       this.modalService.hide();
     });
   };
 
   // Method to Test Primary and Secondary Host Connection.
   testHostConnection = () => {
     let event = {
       "action": "testDBConnection",
       "host": this.popupPrimarySelected ? this.dataSource.primaryHost : this.dataSource.secondaryHost,
       "port": this.popupPrimarySelected ? this.dataSource.primaryPort : this.dataSource.secondaryPort,
       "username": this.popupPrimarySelected ? this.dataSource.primaryUsername : this.dataSource.secondaryUsername,
       "password": this.popupPrimarySelected ? this.dataSource.primaryPassword : this.dataSource.secondaryPassword,
       "dbClusterIdentifier": this.popupPrimarySelected ? this.dataSource.primaryDatabaseName : this.dataSource.secondaryDatabaseName
     }
     this._apiService
       .makeAPICall(event)
       .subscribe((response) => {
         console.log('testdbconnection', response);
         if (this.popupPrimarySelected) {
           this.datasourcePriTestResult = response.data === true ? 'true' : 'false';
         }
         else {
           this.datasourceSecTestResult = response.data === true ? 'true' : 'false';
         }
       });
   };
 
   private activeTabIndex = -1;
 
   onTabSelected(event: any) {
     this.activeTabIndex = event.index;
   }
 
   isActive(index: number): boolean {
     this.borderprimaryActive = true;
     this.activeTabIndex === index;
     return true;
   }
 
   getStepperClasses(index: number): object {
     return {
       'bg-primary border-primary': index <= this.active,
       'surface-border': index > this.active
     };
   }
 
   openAddEditPopup() {
     this.resetAddEditPopup();
     if (this.isEdit) {
       this.currentStep = 1;
       const steps = this.elRef.nativeElement.querySelectorAll('.step');
 
       steps.forEach((step: any, index: any) => {
         if (index === 0) {
           this.renderer.addClass(step, 'editing');
           this.renderer.removeClass(step, 'done');
         } else {
           this.renderer.addClass(step, 'done');
           this.renderer.removeClass(step, 'editing');
         }
       });
 
       console.log('edited data source', this.dataSource);
 
     }
     else {
       this.dataSource = this.dataSource = Object.assign({}, this.dataSourceModel);
       console.log('Add data source', this.dataSource);
     }
   }
 
   resetAddEditPopup() {
     this.datasourcePriTestResult = 'none';
     this.datasourceSecTestResult = 'none';
     this.showErrors = false;
     this.basicFieldsValid = false;
     this.configFieldValid = false;
     this.popupPrimarySelected = true;
     this.currentStep = 2;
     this.basicDetailspage = false;
     this.configPage = false;
     this.sqlTypeSearchValue = '';
     this.backStep(1);
   }
 
 
 
   closeAddEditPopup() {
     this.resetAddEditPopup();
   }
 
   captureactive(value: any) {
     console.log("what is the value:", value)
   }
 
   TestConnection() {
     this.spinnerTriggered = true;
     setTimeout(() => {
       this.spinnerTriggered = false;
 
     }, 3000)
   }
 
   mapEditedData(editedData: any) {
     this.dataSource = this.dataSource = Object.assign({}, this.dataSourceModel);
     this.dataSource = {
       action: 'editDataSource',
       id: editedData.id,
       name: editedData.name,
       type: editedData.type,
       primaryHost: editedData.prihost,
       primaryPort: editedData.priport,
       primaryDatabaseName: editedData.pridatabasename,
       primaryUsername: editedData.priusername,
       primaryPassword: editedData.pripassword,
       secondaryHost: editedData.sechost,
       secondaryPort: editedData.secport,
       secondaryDatabaseName: editedData.secdatabasename,
       secondaryUsername: editedData.secusername,
       secondaryPassword: editedData.secpassword,
       isEnable: editedData.enabled,
       timezone: editedData.timezone,
       description: editedData.description,
       failsafe: editedData.failsafe,
       status: "active", // 
       userid: 1 // 
     };
   }

 

  // Method to Get All Datasource from Database.
  // getAllScheduler = () => {
  //   this._apiService
  //     .makeAPICall({
  //       action: 'getAllScheduler',
  //       searchKey: this.searchKey,
  //     })
  //     .subscribe((response) => {
  //       if (response && response.status) {
  //         if (response.data.length > 0) {
  //           this.schedulerList = response.data;
  //           this._toastr.success('Data Fetched Successfully');
  //         } else {
  //           this._toastr.warning('No Data Found');
  //         }
  //       }
  //     });
  // };

  // Insert Datasource if type is 'add' else updates the existing report builder.
  // Clone existing Datasource and create a new one
  storeScheduler = (type: string) => {
    let action = type === 'add' ? 'addDataSource' : 'editDataSource';
    this._apiService
      .makeAPICall({ action: action })
      .subscribe((response) => {});
  };

  // Method to Delete Datasource from Database.
  deleteScheduler = () => {
    let obj = {};
    this._apiService.makeAPICall(obj).subscribe((response) => {});
  };

  // Method to check cron expression is correct or not
  verifyCronExpression = () => {};

  ngOnDestroy(): void {
this.tabsOverviewShow=false;
  }
}
