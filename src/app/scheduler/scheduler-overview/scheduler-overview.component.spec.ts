import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulerOverviewComponent } from './scheduler-overview.component';

describe('SchedulerOverviewComponent', () => {
  let component: SchedulerOverviewComponent;
  let fixture: ComponentFixture<SchedulerOverviewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SchedulerOverviewComponent]
    });
    fixture = TestBed.createComponent(SchedulerOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
