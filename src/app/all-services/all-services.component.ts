import { Component, OnInit } from '@angular/core';
import { SharedService } from '../services/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-all-services',
  templateUrl: './all-services.component.html',
  styleUrls: ['./all-services.component.scss']
})
export class AllServicesComponent implements OnInit {

  imageSrc: string="../../assets/images/";
  constructor(private sharedService:SharedService,private router:Router){

  }

  ngOnInit(): void {
  
  }

  routerLink(title: string,routepath:string,image:any): void {
    this.sharedService.routeTitle = '';
    this.sharedService.routeImage =''
    this.sharedService.routePath=[];
    this.sharedService.routePath.push(title);
    this.sharedService.routeTitle = title;
    this.sharedService.routeImage = this.imageSrc+image;

    this.router.navigateByUrl('/'+routepath);
  }

}
