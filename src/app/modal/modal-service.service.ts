import {Injectable, ViewChild} from '@angular/core';
import {Models} from './models';
import {Subject} from "rxjs";
import {ModalDirective} from 'ngx-bootstrap/modal';

declare let $: any;


@Injectable({
  providedIn: 'root'
})
export class ModalServiceService {

  constructor() { }
  modalData = new Subject<Models>();

  modalDataEvent = this.modalData.asObservable();

  open(modalData: Models) {
    // alert(JSON.stringify(modalData));
    this.modalData.next(modalData);
  }

}
