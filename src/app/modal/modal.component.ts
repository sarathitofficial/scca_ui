import { Component, OnInit, ViewChild} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ModalServiceService } from './modal-service.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent {
  @ViewChild('myModal1') myModal1!:ModalDirective;
  @ViewChild('myModal2') myModal2!:ModalDirective;
  modalData:any;

  constructor(private modalService: ModalServiceService) { 
    this.modalService.modalDataEvent.subscribe((modalData:any) => {
      this.modalData = modalData;
      if(modalData.type == 'login-modal') {
        this.myModal1.show();
      }
      if(modalData.type == 'other-modal') {
        this.myModal2.show();
      }
    });
  }

  ngOnInit() {
  }
}
