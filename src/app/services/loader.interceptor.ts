import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { InterceptorService } from './interceptor.service';
import { catchError } from 'rxjs/operators';
import { environment } from '../../assets/environments/environment';
 
@Injectable()
export class LoaderInterceptor implements HttpInterceptor {
  isComponentNeedLoader!: boolean;
  constructor(private loaderService: InterceptorService) {}
 
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.isComponentNeedLoader = environment.backgrounderLoader.includes( request.body.action);
    if (!this.isComponentNeedLoader) {
      this.loaderService.showLoader(); // Show the loader when the request starts
 
      return next.handle(request).pipe(
        finalize(() => {
          this.loaderService.hideLoader(); // Hide the loader when the request is completed
        }),
        catchError((error: HttpErrorResponse) => {
          return throwError(error);
        })
      );
    } else {
      return next.handle(request);
    }
  }
}