import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/assets/environments/environment';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class SharedService {

  //string variables
  selectedSortType='createddatedesc';
  routeTitle :any='';
  routeImage : any;
  routePath:any;
  currentDirectoryPath="";
  currentDirectoryParent:any;

  //Boolean variables
  isLoggedInEvent:any;
  showLoader=false;

  //list variables
  screenName: any[] = [];
  sortCategoryList: any[] = [
    { 
      name: 'Name',
      order: 'A-Z',
      key: 'name-asc',
      orderkey : 'asc',
      img: "../../assets/images/nameAtoZ.png" },
    { 
      name: 'Name',
      order:'Z-A',
      key: 'name-desc',
      orderkey : 'desc',
      img: "../../assets/images/nameZtoA.png"},
    { 
      name: 'Created Date',
      order:'ASC',
      key: 'createddate-asc',
      orderkey : 'asc',
      img:"../../assets/images/DateOtoN.png"
     },
    { name: 'Created Date', 
      order:'DESC',
      key: 'createddate-desc',
      orderkey : 'desc',
      img:"../../assets/images/DateNtoO.png" }
];

  constructor(private http: HttpClient) {}

  // Adding headers
  headers = new HttpHeaders()
    .append('content-type', 'application/json')
    .append('Access-Control-Allow-Origin', '*')
    .append('component-name', 'shared')
    .append(
      'Access-Control-Allow-Headers',
      'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'
    );

    
  
  formatDate(sdate: string): string {
    let date=new Date(sdate);
    const year = date.getFullYear();
    const month = (date.getMonth() + 1).toString().padStart(2, '0'); // Months are zero-based
    const day = date.getDate().toString().padStart(2, '0');
    const hours = date.getHours().toString().padStart(2, '0');
    const minutes = date.getMinutes().toString().padStart(2, '0');
    const seconds = date.getSeconds().toString().padStart(2, '0');

    return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
}

sorting(data: any[], field: string, order: string): any[] {
  this.selectedSortType = field;

  // Determine the column to sort by
  const column = field.split('-')[0];

  // Sort the data array based on the specified field and order
  data.sort((a, b) => {
    if (column === 'name') {
      // For names, compare the strings directly
      return order === 'asc'? a[column].localeCompare(b[column]) : b[column].localeCompare(a[column]);
    } else {
      // For dates, use Intl.DateTimeFormat to compare including time
      const dateFormatter = new Intl.DateTimeFormat(undefined, { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit' }).format;
      const formattedDateA = dateFormatter(new Date(a[column]));
      const formattedDateB = dateFormatter(new Date(b[column]));
      return order === 'asc'? formattedDateA.localeCompare(formattedDateB) : formattedDateB.localeCompare(formattedDateA);
    }
  });

  // Return the sorted data array
  return data;
}


  // get all screen name from screen database
  getAllScreenDetails = (screenNameObject: any) => {
    return this.http.post<any>(environment.apiUrl, screenNameObject, {
      headers: this.headers,
    });
  };

  //login details
  getloginDetails(isLoggedIn:any){
    this.isLoggedInEvent = isLoggedIn;
    console.log("service event:",this.isLoggedInEvent);
    return this.isLoggedInEvent;
  }


}
