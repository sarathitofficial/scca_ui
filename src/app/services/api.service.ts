import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../assets/environments/environment';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class ApiService {
  url = environment.apiUrl;
  constructor(private http: HttpClient) {}
  // Adding headers
  headers = new HttpHeaders()
    .append('content-type', 'application/json')
    .append('Access-Control-Allow-Origin', '*')
    .append(
      'Access-Control-Allow-Headers',
      'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'
    );

  //Returns all the data sources.
  makeAPICall = (event: any): Observable<any> => {
    console.log('environment',this.url);
    return this.http.post<any>(this.url, event);
  };

    // Asynchronous version of makeAPICall
    async makeAPICallAsync(event: any): Promise<any> {
      console.log('environment', this.url);
      return this.http.post<any>(this.url, event).toPromise();
    }
  
}
