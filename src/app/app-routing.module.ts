import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfigurationComponent } from './configuration/configuration.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { SmtpComponent } from './smtp/smtp.component';
import { SsoComponent } from './sso/sso.component';
import { UserComponent } from './user/user.component';
import { HomeComponent } from './home/home.component';
import { UserOrganizationComponent } from './user-organization/user-organization.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { FolderOverviewComponent } from './dashboard/folder-overview/folder-overview.component';
import { DashboardOverviewComponent } from './dashboard/dashboard-overview/dashboard-overview.component';
import { ReportComponent } from './report/report.component';
import { DatasourceComponent } from './datasource/datasource.component';
import { ReportBuilderComponent } from './report-builder/report-builder.component';
import { BuilderOverviewComponent } from './report-builder/builder-overview/builder-overview.component';
import { SubFolderComponent } from './report-builder/sub-folder/sub-folder.component';
import { OverviewComponent } from './report-builder/overview/overview.component';
import { CollectionComponent } from './collection/collection.component';
import { FolderComponent } from './report/folder/folder.component';
import { SubfolderComponent } from './report/subfolder/subfolder.component';
import { AdminUsersComponent } from './admin-users/admin-users.component';
import { GroupComponent } from './group/group.component';
import { PermissionComponent } from './permission/permission.component';
import { AuditDataComponent } from './audit-data/audit-data.component';
import { LoginLogoutAuditComponent } from './login-logout-audit/login-logout-audit.component';
import { SchedulerAuditComponent } from './scheduler-audit/scheduler-audit.component';
import { SchedulerComponent } from './scheduler/scheduler.component';
const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
  },
  {
    path:'login',
    component: LoginComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'Users',
    component: UserComponent,
    children: [
      {
        path: 'Profile',
        component: UserProfileComponent,
      },
      {
        path: 'Organization',
        component: UserOrganizationComponent,
      },
    ],
  },
  {
    path: 'configurations',
    component: ConfigurationComponent,
    children: [
      {
        path: 'sso',
        component: SsoComponent,
        pathMatch: 'full',
      },
      {
        path: 'smtp',
        component: SmtpComponent,
        pathMatch: 'full',
      },
    ],
  },
  {
    path: 'Dashboard',
    component: DashboardComponent,
    pathMatch: 'full',
  
},  
{
  path: 'Dashboard/:foldername',
  component: DashboardOverviewComponent,
  pathMatch: 'full',
}, {
  path: 'Dashboard/:foldername/:name',
  component: FolderOverviewComponent,
  pathMatch:'full'
},{
  path:'Datasource',
  component:DatasourceComponent,
  pathMatch:'full'
},
{
  path:'Collections',
  component:CollectionComponent,
  pathMatch:'full'
},
{
  path:'Reports',
  component:ReportComponent,
  pathMatch:'full'
},
{
  path:'Reports/:foldername',
  component:FolderComponent,
  pathMatch:'full'
},
{
  path:'Reports/:foldername/Reports-folder',
  component:SubfolderComponent,
  pathMatch:'full'
},

{
  path:'Report-Builder',
  component:ReportBuilderComponent,
  pathMatch:'full'
},
{
  path:'Report-Builder/:foldername',
  component:BuilderOverviewComponent,
  pathMatch:'full'
},{
  path:'Report-Builder/:foldername/Report-Builder-Folder',
  component:SubFolderComponent,
  pathMatch:'full'
},{
  path:'Admin-Users',
  component:AdminUsersComponent,
  pathMatch:'full'
},{
  path:'Admin-Groups',
  component:GroupComponent,
  pathMatch:'full'
},{
  path:'Admin-Permissions',
  component:PermissionComponent,
  pathMatch:'full'
},{
  path:'Audit-Data',
  component:AuditDataComponent,
  pathMatch:'full'
},{
  path:'Audit-Login-Logout',
  component:LoginLogoutAuditComponent,
  pathMatch:'full'
},{
  path:'Audit-Scheduler',
  component:SchedulerAuditComponent,
  pathMatch:'full'
},{
  path:'Scheduler',
  component:SchedulerComponent,
  pathMatch:'full'
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
