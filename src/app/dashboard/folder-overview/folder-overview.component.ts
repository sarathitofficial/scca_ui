import { Component,ElementRef,OnInit, Renderer2, ViewChild,OnDestroy, NgZone  } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import * as Chart from 'chart.js';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
declare var myExtObject: any;


interface ITab {
  title: string;
  content: string;
  removable: boolean;
  disabled: boolean;
  active?: boolean;
  customClass?: string;
}

interface City {
  name: string,
  code: string
}



@Component({
  selector: 'app-folder-overview',
  templateUrl: './folder-overview.component.html',
  styleUrls: ['./folder-overview.component.scss'],
  
})
export class FolderOverviewComponent implements OnInit, OnDestroy {
  @ViewChild('fullscreenDiv') fullscreenDiv: ElementRef | undefined;
   @ViewChild('fullScreen',{read:ElementRef}) divRef!: ElementRef<any>;
  @ViewChild('open',{read: ElementRef})closeRef!:ElementRef<any>;
  @ViewChild('close',{read: ElementRef})modelRef!:ElementRef<any>;

  @ViewChild('myCanvas')
  public canvas!: ElementRef;
  public context!: CanvasRenderingContext2D;
  public chartType: string = 'line';
  public chartData!: any[];
  public chartLabels!: any[];
  public chartColors!: any[];
  public chartOptions: any;


  private chart: am4charts.XYChart | undefined;

  cities!: City[];
selectedQueues!:any;
  selectedCities!: City[];
  selectedTypes!: any;

  rightfullscreendiv!:any;

constructor(private cdr: ChangeDetectorRef, private zone: NgZone,
  private router:Router,private renderer: Renderer2, private el: ElementRef){
    am4core.useTheme(am4themes_animated);
  setInterval(() => {
  this.rightfullscreendiv = this.fullscreenDiv;
  }, 2000);

  this.cities = [
    {name: 'Outcome', code: 'OC'},
    {name: 'User', code: 'U'},
    {name: 'Queue', code: 'Q'},
    {name: 'Wrapup', code: 'W'},
    {name: 'Flow', code: 'Fl'}
];

this.selectedQueues = [
  {name: 'Outcome', code: 'OC'},
  {name: 'User', code: 'U'},
  {name: 'Queue', code: 'Q'},
  {name: 'Wrapup', code: 'W'},
  {name: 'Flow', code: 'Fl'}
]

}

activeIndex: number | undefined = 0;
nodes!: any;
foldername:string = 'Agent';
editScreen:boolean=false;
sidebarVisible: boolean = false;
selectedQueries!:any;
typeformGroup!: FormGroup;
selectedCategory!:any;
Usersselected: boolean = false;
Wrapupselected: boolean = false;
Flowselected: boolean = false;
Outcomeselected: boolean = false;
Queueselected:boolean = false;
metricsSelected:boolean = false;
webContentSelected: boolean=false;
TextSelected: boolean=false;
chartsSelected: boolean=false;
globalWidgetIdentifier:any;
fullscreenelement:any;
categories: any[] = [
    { name: 'Metric', key: 'M' },
    { name: 'Chart', key: 'C' },
    { name: 'Text', key: 'T' },
    { name: 'WebContent', key: 'W' }
];
foldernames = [{
  name: 'Agent Statistics',
}];

activeIndexChange(index : any){
    this.activeIndex = index
}

ngAfterViewInit() {
  
  this.cdr.detectChanges();
}

ngAfterContentInit(){
  // console.log("divref is:",this.divRef);
}
 ngOnInit(): void {

  window.localStorage.removeItem('selectedWidget');
  this.typeformGroup = new FormGroup({
    key: new FormControl()
});



 }

 elem = document.documentElement;
 fullscreen(){
  if(this.elem.requestFullscreen){
    this.elem.requestFullscreen();
  }
 }



toggleFullscreen() {
  if (this.rightfullscreendiv) { // Check if divRef is defined
    const elem = this.rightfullscreendiv.nativeElement;
    try {
      if (elem.requestFullscreen) {
        elem.requestFullscreen();
      } else if (elem.msRequestFullscreen) {
        elem.msRequestFullscreen();
      } else if (elem.mozRequestFullScreen) {
        elem.mozRequestFullScreen();
      } else if (elem.webkitRequestFullscreen) {
        elem.webkitRequestFullscreen();
      }
    } catch (error) {
      console.error('Failed to enter fullscreen:', error);
      // Optionally, inform the user that fullscreen couldn't be enabled
    }
  } else {
    console.error('divRef is undefined');
  }
}

 closefullscreen(){
  if(document.exitFullscreen){
    document.exitFullscreen();
  }
 }


 filemanagement(filemanagement:any){
  this.router.navigate(['/Dashboard',filemanagement])
 }

 backPage(foldername:any){
  this.router.navigate(['/Dashboard',foldername])
 }

 openModal(){
  this.modelRef.nativeElement.click()
 }
 closeModal(){
  this.closeRef.nativeElement.click()
 }

 addwidget(value:any,widget:any){
  window.localStorage.setItem('selectedWidget','')
  console.log("add widget",value,widget);
  this.sidebarVisible = true;
  window.localStorage.setItem('selectedWidget',value);
  this.globalWidgetIdentifier = window.localStorage.getItem('selectedWidget');
 }


 editDashboard(foldername:any){
  console.log("edit dashboard", foldername);
this.editScreen=true;
 }

  tabs: ITab[] = [
    {
      title: 'Agent 1',
      content: 'Dynamic content 1',
      removable: false,
      disabled: false,
    },
    {
      title: 'Agent Details',
      content: 'Dynamic content 2',
      removable: false,
      disabled: false,
    },
    {
      title: 'Wrapup',
      content: 'Dynamic content 3',
      removable: true,
      disabled: false,
    },
  ];



  addNewTab(): void {
    const newTabIndex = this.tabs.length + 1;
    this.tabs.push({
      title: `${newTabIndex}`,
      content: ` ${newTabIndex}`,
      disabled: false,
      removable: true,
    });
    this.closeModal();
  }

  removeTabHandler(tab: ITab): void {
    this.tabs.splice(this.tabs.indexOf(tab), 1);
    console.log('Remove Tab handler');
  }

  public widgets:any[] = [
    {
      name: 'Note 1',
      hasConfigured: false
    },
    {
      name: 'Note 2', hasConfigured: false
    },

  ];

  public widgetsrectangle:any[] = [
    {
      name: 'Note 1',
      hasConfigured: false
    },
    {
      name: 'Note 2', hasConfigured: false
    },

  ];

  returnUpdatedList(data:any) {
    this.widgets = data;
  }

  onRadioChange(event:any){
    this.selectedTypes = event;
    console.log("selected Types",this.selectedTypes);
    if(this.selectedTypes){
      switch(this.selectedTypes){
        case 'M':
        this.metricsSelected = true;
        this.webContentSelected = false;
            this.TextSelected = false;
            this.chartsSelected = false;
        break;
        case 'C':
          this.chartsSelected = true;
          this.webContentSelected = false;
          this.TextSelected = false;
          this.metricsSelected = false;
        break;  
        case 'T':
          this.TextSelected = true;
          this.webContentSelected = false;
          this.chartsSelected = false;
            this.metricsSelected = false;
          break;
        case 'W':
          this.webContentSelected = true;
          this.TextSelected = false;
            this.chartsSelected = false;
            this.metricsSelected = false;
          break;
          default:
            this.webContentSelected = false;
            this.TextSelected = false;
            this.chartsSelected = false;
            this.metricsSelected = false;
            break;
      }

    }

  }

  onChangefilter(event:any){

    this.selectedQueries = event.itemValue;
    if(this.selectedQueries){
      switch(this.selectedQueries.code){
        case 'OC':
         this.Outcomeselected=true;
         this.Usersselected = false;
         this.Queueselected = false;
         this.Wrapupselected = false;
         this.Flowselected = false;
          break;
        case 'U':
         this.Usersselected = true;
         this.Outcomeselected=false;
         this.Queueselected = false;
         this.Wrapupselected = false;
         this.Flowselected = false;
          break;
        case 'Q':
          this.Outcomeselected=false;
          this.Usersselected = false;
          this.Queueselected = true;
          this.Wrapupselected = false;
          this.Flowselected = false;
          break;
        case 'W':
        this.Wrapupselected = true;
        this.Flowselected = false;
        this.Outcomeselected=false;
        this.Usersselected = false;
        this.Queueselected = false;
          break;
        case 'Fl':
          this.Flowselected = true;
          this.Outcomeselected=false;
          this.Usersselected = false;
          this.Queueselected = false;
          this.Wrapupselected = false;
          break;
        default:
          this.Outcomeselected=false;
          this.Usersselected = false;
          this.Queueselected = false;
          this.Wrapupselected = false;
          this.Flowselected = false;
          break;
      }
    }
  }
  
  addChart(index: number,selectedTypes:any) {
    console.log('index is:',index + 'selectedType is:',selectedTypes);
    const widget = this.widgets[index];
    widget.hasConfigured = true;
    if(selectedTypes==='C'){
      //bar charts
      this.zone.runOutsideAngular(() => {
      let chart = am4core.create('chartdiv', am4charts.XYChart);

  // Add data
  chart.data = [
    {
      category: 'Category 1',
      value: 50,
    },
    {
      category: 'Category 2',
      value: 35,
    },
    {
      category: 'Category 1',
      value: 50,
    },
    {
      category: 'Category 2',
      value: 35,
    },
    {
      category: 'Category 3',
      value: 50,
    },
    {
      category: 'Category 4',
      value: 35,
    },
    {
      category: 'Category 5',
      value: 50,
    },
    {
      category: 'Category 6',
      value: 35,
    },
    {
      category: 'Category 7',
      value: 50,
    },
    {
      category: 'Category 8',
      value: 35,
    },
  ];

  // Create axes
  let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
  categoryAxis.dataFields.category = 'category';

  let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

  // Create series
  let series = chart.series.push(new am4charts.ColumnSeries());
  series.dataFields.valueY = 'value';
  series.dataFields.categoryX = 'category';

  this.chart = chart;
});  

  //     //piecharts
   
  // this.piechartcanvas();
  //   //LINE CHARTS
  //   this.linchartcanvas()
    }
    if(selectedTypes==='M'){
      
    }
    if(selectedTypes==='W'){
      
    }
    if(selectedTypes==='T'){
      
    }
    
    this.sidebarVisible=false;
  }

  piechartcanvas(){
    let chart = am4core.create("chartdiv", am4charts.PieChart);
    // Add data
chart.data = [{
"country": "Lithuania",
"litres": 501.9
}, {
"country": "Czech Republic",
"litres": 301.9
}, {
"country": "Ireland",
"litres": 201.1
}, {
"country": "Germany",
"litres": 165.8
}, {
"country": "Australia",
"litres": 139.9
}, {
"country": "Austria",
"litres": 128.3
}, {
"country": "UK",
"litres": 99
}, {
"country": "Belgium",
"litres": 60
}, {
"country": "The Netherlands",
"litres": 50
}];

// Add and configure Series
let pieSeries = chart.series.push(new am4charts.PieSeries());
pieSeries.dataFields.value = "litres";
pieSeries.dataFields.category = "country";
  }

  linchartcanvas(){
    let chart = am4core.create("chartdiv", am4charts.XYChart);

    let data = [];
    let value = 50;
    for(var i = 0; i < 300; i++){
      let date = new Date();
      date.setHours(0,0,0,0);
      date.setDate(i);
      value -= Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);
      data.push({date:date, value: value});
    }
    
    chart.data = data;
    
    // Create axes
    let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.renderer.minGridDistance = 60;
    
    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    
    // Create series
    let series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.valueY = "value";
    series.dataFields.dateX = "date";
    series.tooltipText = "{value}"
    if(series){
      if(series.tooltip){
        series.tooltip.pointerOrientation = "vertical";
      }
    }
    
    chart.cursor = new am4charts.XYCursor();
    chart.cursor.snapToSeries = series;
    chart.cursor.xAxis = dateAxis;
    
    //chart.scrollbarY = new am4core.Scrollbar();
    chart.scrollbarX = new am4core.Scrollbar();
  }

  visible: boolean = false;

  showdashboardDialog() {
      this.visible = true;
  }
  ngOnDestroy() {
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }
 
  
}
