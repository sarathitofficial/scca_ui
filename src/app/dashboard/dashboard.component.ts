import { Component,ElementRef,OnInit, ViewChild  } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from '../services/shared.service';

interface ITab {
  title: string;
  content: string;
  removable: boolean;
  disabled: boolean;
  active?: boolean;
  customClass?: string;
}
declare let $: any; 
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
  @ViewChild('open',{read: ElementRef})closeRef!:ElementRef<any>;
  @ViewChild('close',{read: ElementRef})modelRef!:ElementRef<any>;
 
constructor(private cdr: ChangeDetectorRef, private router:Router, private sharedService:SharedService){}

activeIndex: number | undefined = 0;



foldername = [{
  name: 'Agent',
},{
  name:'Queue'
},{
  name:'Wrapup'
}];


activeIndexChange(index : any){
    this.activeIndex = index
}

ngAfterViewInit() {
  this.cdr.detectChanges();
}
 ngOnInit(): void {
  this.sharedService.routeTitle = 'Dashoard';     
 }

 filemanagement(foldername:any){
this.router.navigate(['/Dashboard',foldername])
  }
 
  
 

 openModal(){
  this.modelRef.nativeElement.click()
 }
 closeModal(){
  this.closeRef.nativeElement.click()
 }

  tabs: ITab[] = [
    {
      title: 'Dynamic Title 1',
      content: 'Dynamic content 1',
      removable: false,
      disabled: false,
    },
    {
      title: 'Dynamic Title 2',
      content: 'Dynamic content 2',
      removable: false,
      disabled: false,
    },
    {
      title: 'Dynamic Title 3',
      content: 'Dynamic content 3',
      removable: true,
      disabled: false,
    },
  ];



  addNewTab(): void {
    const newTabIndex = this.tabs.length + 1;
    this.tabs.push({
      title: `Dynamic Title ${newTabIndex}`,
      content: `Dynamic content ${newTabIndex}`,
      disabled: false,
      removable: true,
    });
    this.closeModal();
  }

  removeTabHandler(tab: ITab): void {
    this.tabs.splice(this.tabs.indexOf(tab), 1);
    console.log('Remove Tab handler');
  }

  items: string[] = [
    'The first choice!',
    'And another choice for you.',
    'but wait! A third!'
  ];

}
