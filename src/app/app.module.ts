import { NgModule, NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ToastrModule, ToastNoAnimation, ToastNoAnimationModule  } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
// Import statement for component
import { DatasourceComponent } from './datasource/datasource.component';
import { InterceptorService } from './services/interceptor.service';
import { ReportComponent } from './report/report.component';
import { ReportBuilderComponent } from './report-builder/report-builder.component';
import { UserComponent } from './user/user.component';
import { GroupComponent } from './group/group.component';
import { PermissionComponent } from './permission/permission.component';
import { LoginComponent } from './login/login.component';
import { SmtpComponent } from './smtp/smtp.component';
import { SsoComponent } from './sso/sso.component';
import { AuditDataComponent } from './audit-data/audit-data.component';
import { LoginLogoutAuditComponent } from './login-logout-audit/login-logout-audit.component';
import { CollectionComponent } from './collection/collection.component';
import { EmailSchedulerAuditComponent } from './email-scheduler-audit/email-scheduler-audit.component';
import { SchedulerComponent } from './scheduler/scheduler.component';
import { HeaderComponent } from './toolbar/header/header.component';
import { FooterComponent } from './toolbar/footer/footer.component';
import { SidebarComponent } from './toolbar/sidebar/sidebar.component';
import { RouteComponent } from './toolbar/route/route.component';
import { AllServicesComponent } from './all-services/all-services.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TabHeadingDirective, TabsModule } from 'ngx-bootstrap/tabs';
import { DropdownModule } from 'primeng/dropdown';
import { SplitterModule } from 'primeng/splitter';
import { TabViewModule } from 'primeng/tabview';
import { AccordionModule } from 'primeng/accordion';
import { SidebarModule } from 'primeng/sidebar';
import { MultiSelectModule } from 'primeng/multiselect';
import { DialogModule } from 'primeng/dialog';
import { MenuItem, MessageService } from 'primeng/api';
import { StepsModule } from 'primeng/steps';
import { ToastModule } from 'primeng/toast';
import { TreeNode } from 'primeng/api';
import { TreeModule } from 'primeng/tree';
import { ButtonModule } from 'primeng/button';
import { DragDropModule } from 'primeng/dragdrop';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TabMenuModule } from 'primeng/tabmenu';
import { RippleModule } from 'primeng/ripple';
import { HomeComponent } from './home/home.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserOrganizationComponent } from './user-organization/user-organization.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ToolTipComponent } from './tool-tip/tool-tip.component';
import { LanguageComponent } from './language/language.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { MenuDropdownComponent } from './menu-dropdown/menu-dropdown.component';
import { BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { ModalComponent } from './modal/modal.component';
import { ChartModule } from 'primeng/chart';
import { DragDropSwapDirective } from './dashboard/folder-overview/drag-drop-swap.directives';
import { FolderOverviewComponent } from './dashboard/folder-overview/folder-overview.component';
import { DashboardOverviewComponent } from './dashboard/dashboard-overview/dashboard-overview.component';
import { BarchartComponent } from './charts/barchart/barchart.component';
import { LinechartComponent } from './charts/linechart/linechart.component';
import { PiechartComponent } from './charts/piechart/piechart.component';
import { InputSwitchModule } from 'primeng/inputswitch';
import { RouteLayoutComponent } from './route-layout/route-layout.component';
import { BasicDetailsComponent } from './datasource/basic-details/basic-details.component';
import { ConfigurationsComponent } from './datasource/configurations/configurations.component';
import { BuilderOverviewComponent } from './report-builder/builder-overview/builder-overview.component';
import { SubFolderComponent } from './report-builder/sub-folder/sub-folder.component';
import { OverviewComponent } from './report-builder/overview/overview.component';
import { LoaderInterceptor } from './services/loader.interceptor';
import { FolderComponent } from './report/folder/folder.component';
import { SubfolderComponent } from './report/subfolder/subfolder.component';
import { BsDatepickerModule,BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { TableModule } from 'primeng/table';
import { PickListModule } from 'primeng/picklist';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DatatableComponent } from './report/datatable/datatable.component';
import { AdminUsersComponent } from './admin-users/admin-users.component';
import { SchedulerAuditComponent } from './scheduler-audit/scheduler-audit.component';
import { SchedulerOverviewComponent } from './scheduler/scheduler-overview/scheduler-overview.component';  
@NgModule({
  declarations: [
    AppComponent,
    DatasourceComponent,
    ReportComponent,
    ReportBuilderComponent,
    UserComponent,
    GroupComponent,
    PermissionComponent,
    LoginComponent,
    SmtpComponent,
    SsoComponent,
    AuditDataComponent,
    LoginLogoutAuditComponent,
    CollectionComponent,
    EmailSchedulerAuditComponent,
    SchedulerComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    RouteComponent,
    AllServicesComponent,
    ConfigurationComponent,
    DashboardComponent,
    HomeComponent,
    UserProfileComponent,
    UserOrganizationComponent,
    UserDetailsComponent,
    LandingPageComponent,
    NavbarComponent,
    ToolTipComponent,
    LanguageComponent,
    MenuDropdownComponent,
    ModalComponent,SubFolderComponent,
    FolderOverviewComponent,DragDropSwapDirective, DashboardOverviewComponent, BarchartComponent, LinechartComponent, PiechartComponent, RouteLayoutComponent, BasicDetailsComponent, ConfigurationsComponent, BuilderOverviewComponent, OverviewComponent, FolderComponent,SubfolderComponent, DatatableComponent, AdminUsersComponent, SchedulerAuditComponent, SchedulerOverviewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,TabMenuModule,RippleModule,StepsModule, ToastModule,TableModule,PickListModule,NgxDatatableModule,AccordionModule,
    TabsModule,RadioButtonModule,MultiSelectModule,ChartModule,DialogModule,ButtonModule,InputSwitchModule,
    DropdownModule,SplitterModule,TabViewModule,AccordionModule,TreeModule,DragDropModule,SidebarModule,
    ReactiveFormsModule, BrowserAnimationsModule,CommonModule,BsDropdownModule.forRoot(),ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ToastrModule.forRoot()
  ],

  providers: [BsModalService,BsModalService,MessageService,BsDatepickerConfig,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA], 
})
export class AppModule {}
