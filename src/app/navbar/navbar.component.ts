import { Component } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Router } from '@angular/router';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent {
  countries: any[] | undefined;

  selectedCity: any;

  isDarkTheme: boolean = false; // Initial theme state

  items!: MenuItem[];

  constructor(private router: Router) {}
  ngOnInit() {}

  toggleTheme(): void {
    this.isDarkTheme = !this.isDarkTheme;
    this.setTheme(this.isDarkTheme);
    // Store theme preference in localStorage (optional)
    localStorage.setItem('theme', this.isDarkTheme ? 'dark' : 'light');
  }

  setTheme(isDark: boolean): void {
    document.body.classList.toggle('dark-theme', isDark);
  }
}
