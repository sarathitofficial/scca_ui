import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuilderOverviewComponent } from './builder-overview.component';

describe('BuilderOverviewComponent', () => {
  let component: BuilderOverviewComponent;
  let fixture: ComponentFixture<BuilderOverviewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BuilderOverviewComponent]
    });
    fixture = TestBed.createComponent(BuilderOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
