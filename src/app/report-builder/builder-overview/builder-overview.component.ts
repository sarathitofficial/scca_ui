import { Component, ElementRef, OnInit, Renderer2, ViewChild, OnDestroy, NgZone } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';
import { MessageService } from 'primeng/api';
import { MenuItem } from 'primeng/api';
import { StepsModule } from 'primeng/steps';
import { Subscription } from 'rxjs';
// Shared service import
import { TicketstepperService } from 'src/app/services/ticketstepper.service';
import { SharedService } from 'src/app/services/shared.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

interface DataSourceCard {
  [x: string]: any;
  DataSourceName: string;
  DataSoureType: string;
  Databasename: string;
  hostname: string;
  dateString: string;
  // Add any other properties you need
}



@Component({
  selector: 'app-builder-overview',
  templateUrl: './builder-overview.component.html',
  styleUrls: ['./builder-overview.component.scss']
})
export class BuilderOverviewComponent {

  @ViewChild('open', { read: ElementRef }) closeRef!: ElementRef<any>;
  @ViewChild('close', { read: ElementRef }) modelRef!: ElementRef<any>;
  // String type variables
  searchKey: string = '';
  DataSourceName: string = "Agent"
  DataSoureType: string = 'PgSQL';
  Databasename: string = 'PgSQL';
  hostname: string = '127.0.0.1';
  checked: boolean = false;
  // List type variables
  dataSourceList: any[] = [];
  formattedDate: any;
  dateString: string = "2024-03-10T 10:10:10"; // Note: Use 'T' instead of space for ISO 8601 compliance
  dateObject = new Date(this.dateString);
  borderprimaryActive: boolean = false;
  datasourceitems: any
  datasourceactiveItem: any | undefined;

  currentStep = 1;
  numSteps = 2;
  configPage: boolean = false;
  basicDetailspage: boolean = false;
  spinnerTriggered: boolean = false;
  // Example data array
  dataSourceCards: DataSourceCard[] = [
    {
      DataSourceName: "Data 0",
      DataSoureType: "Primary",
      Databasename: "DB1",
      hostname: "localhost",
      dateString: "2023-04-01T14:30:00Z"
    },
    {
      DataSourceName: "Data 0",
      DataSoureType: "Secondary",
      Databasename: "DB2",
      hostname: "dbserver.example.com",
      dateString: "2023-04-02T09:45:00Z"
    },
    {
      DataSourceName: "Data 0",
      DataSoureType: "Primary",
      Databasename: "DB3",
      hostname: "database3.local",
      dateString: "2023-04-03T18:15:00Z"
    }
  ];

  index = 0;
  active = 0;
  items: MenuItem[] | undefined;

  constructor(
    private _toastr: ToastrService, private _apiService: ApiService,
    private _sharedService: SharedService, private router: Router,
    public messageService: MessageService, private renderer: Renderer2, private elRef: ElementRef,
    public ticketService: TicketstepperService, private _formBuilder: FormBuilder
  ) { }

  firstFormGroup = this._formBuilder.group({
    firstCtrl: ['', Validators.required],
  });
  secondFormGroup = this._formBuilder.group({
    secondCtrl: ['', Validators.required],
  });

  nameAtoZ = "../../assets/images/nameAtoZ.png";
  nameZtoA = "../../assets/images/nameZtoA.png";
  DateOtoN = "../../assets/images/DateOtoN.png";
  DateNtoO = "../../assets/images/DateNtoO.png";
  categories: any[] = [
    {
      name: 'Name (A to Z)',
      key: 'A',
      img: "../../assets/images/nameAtoZ.png"
    },
    {
      name: 'Name (Z to A)',
      key: 'Z',
      img: "../../assets/images/nameZtoA.png"
    },
    {
      name: 'Date (Oldest to Newest)',
      key: 'O',
      img: "../../assets/images/DateOtoN.png"
    },
    {
      name: 'Date (Newest to Oldest)',
      key: 'N',
      img: "../../assets/images/DateNtoO.png"
    }
  ];

  foldername = [{
    name: 'Realtime',
  }, {
    name: 'Historical',
  }];



  ngOnInit = () => {
    this.clear();

    function formatDate(date: Date): string {
      const year = date.getFullYear();
      const month = (date.getMonth() + 1).toString().padStart(2, '0'); // Months are zero-based
      const day = date.getDate().toString().padStart(2, '0');
      const hours = date.getHours().toString().padStart(2, '0');
      const minutes = date.getMinutes().toString().padStart(2, '0');
      const seconds = date.getSeconds().toString().padStart(2, '0');

      return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
    }




    this.formattedDate = formatDate(this.dateObject);
    console.log(this.formattedDate);



    this.dataSourceList = [
      {
        "id": 1,
        "name": "Datasource one",
        "type": "oracle",
        "prihost": "srvipdevdb.ctg6a8i8y5kx.us-east-1.rds.amazonaws.com",
        "priport": "5432",
        "pridatabasename": "scca_dev",
        "priusername": "postgres",
        "pripassword": "Srv$Engg24",
        "sechost": "100.10.10.12",
        "secport": "4444",
        "secdatabasename": "userdb",
        "secusername": "user",
        "secpassword": "user",
        "enabled": true,
        "timezone": null,
        "description": "description for datasource",
        "failsafe": true,
        "createdby": "mohan",
        "modifieddate": "2024-06-06T07:22:44.474016",
        "createddate": "2024-06-06T07:17:22.112019",
        "modifiedby": "mohan",
        "testresult": {
          "status": true,
          "messgae": "Db Connection successfull",
          "data": true
        },
        "connecteddatasource": "primary"
      }
    ]

  };

  onRadioChange(key: any) {

  }

  nextStep(): void {
    this.currentStep++;
    if (this.currentStep > this.numSteps) {
      this.currentStep = 1;
    }
    const steps = this.elRef.nativeElement.querySelectorAll('.step');

    steps.forEach((step: any, index: any) => {
      const stepNum = index + 1;
      if (stepNum === this.currentStep) {
        this.renderer.addClass(step, 'editing');
      } else {
        this.renderer.removeClass(step, 'editing');
      }
      if (stepNum < this.currentStep) {
        this.renderer.addClass(step, 'done');
      } else {
        this.renderer.removeClass(step, 'done');
      }
    });
    this.basicDetailspage = false;
    this.configPage = true;
  }

  backStep(step: any) {
    if (this.currentStep > 1) {
      this.currentStep--;
    } else {
      this.currentStep = this.numSteps;
    }
    const steps = this.elRef.nativeElement.querySelectorAll('.step');

    steps.forEach((step: any, index: any) => {
      const stepNum = index + 1;
      if (stepNum === this.currentStep) {
        this.renderer.addClass(step, 'editing');
      } else {
        this.renderer.removeClass(step, 'editing');
      }
      if (stepNum < this.currentStep) {
        this.renderer.addClass(step, 'done');
      } else {
        this.renderer.removeClass(step, 'done');
      }
    });
    this.basicDetailspage = true;
    this.configPage = false;
  }

  // Method to reset/clear all fields.
  clear = () => {
    this.dataSourceList = [];
    this.searchKey = '';

    this.getAllDatasource();
  };

  // Method to call when search icon is pressed.
  search = () => {
    this.getAllDatasource();
  };

  // On change in sorting option.
  onSortChange = (field: string, order: string) => {
    this.dataSourceList = this._sharedService.sorting(
      this.dataSourceList,
      field,
      order
    );
  };

  // Method to Get All Datasource from Database.
  getAllDatasource = () => {
    this._apiService
      .makeAPICall({
        action: 'getAllDataSource',
        searchKey: this.searchKey,
      })
      .subscribe((response: any) => {
        if (response && response.status) {
          if (response.data.length > 0) {
            this.dataSourceList = response.data;
            this._toastr.success('Data Fetched Successfully');
          } else {
            this._toastr.warning('No Data Found');
          }
        }
      });
  };

  // Insert Datasource if type is 'add' else updates the existing report builder.
  // Clone existing Datasource and create a new one
  storeDataSource = (type: string) => {
    let action =
      type === 'add'
        ? 'addDataSource'
        : type === 'edit'
          ? 'editDataSource'
          : 'cloneDataSource';
    this._apiService
      .makeAPICall({ action: action })
      .subscribe((response: any) => { });
  };

  // Method to Delete Datasource from Database.
  deleteDatasource = () => {
    let obj = {};
    this._apiService.makeAPICall(obj).subscribe((response: any) => { });
  };

  // Method to Test Primary and Secondary Host Connection.
  testHostConnection = () => {
    this._apiService
      .makeAPICall({ action: 'testDBConnection' })
      .subscribe((response: any) => { });
  };

  private activeTabIndex = -1;

  onTabSelected(event: any) {
    this.activeTabIndex = event.index;
  }

  isActive(index: number): boolean {
    this.borderprimaryActive = true;
    this.activeTabIndex === index;
    return true;
  }

  getStepperClasses(index: number): object {
    return {
      'bg-primary border-primary': index <= this.active,
      'surface-border': index > this.active
    };
  }
  openModal() {
    this.modelRef.nativeElement.click();
    this.basicDetailspage = true;
  }
  closeModal() {
    this.closeRef.nativeElement.click();
    this.basicDetailspage = false;

  }

  captureactive(value: any) {
    console.log("what is the value:", value)
  }

  TestConnection() {
    this.spinnerTriggered = true;
    setTimeout(() => {
      this.spinnerTriggered = false;

    }, 3000)
  }

  //REaltime,Historical subfolder Route navigation

  filemanagement(folderName: any) {
    this._sharedService.currentDirectoryParent=0;
    this._sharedService.currentDirectoryPath+=(folderName+'/');
    this.router.navigate(['/Report-Builder', folderName, 'Report-Builder-Folder'])
  }
}
