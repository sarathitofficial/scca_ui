import { Component, ElementRef, OnInit, Renderer2, ViewChild, OnDestroy, NgZone, TemplateRef, ViewContainerRef } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';
import { MessageService } from 'primeng/api';
import { MenuItem } from 'primeng/api';
import { StepsModule } from 'primeng/steps';
import { Observable, Subscription } from 'rxjs';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { HttpClient } from '@angular/common/http';
// Shared service import
import { SharedService } from 'src/app/services/shared.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalService ,BsModalRef} from 'ngx-bootstrap/modal';

interface DataSourceCard {
  [x: string]: any;
  DataSourceName: string;
  DataSoureType: string;
  Databasename: string;
  hostname: string;
  dateString: string;
  // Add any other properties you need
}

interface country {
  name?: string;
  code?: string;
}

interface ITab {
  title: string;
  content: string;
  removable: boolean;
  disabled: boolean;
  active?: boolean;
  customClass?: string;
}

@Component({
  selector: 'app-sub-folder',
  templateUrl: './sub-folder.component.html',
  styleUrls: ['./sub-folder.component.scss']
})
export class SubFolderComponent {
  @ViewChild('openFile', { read: ElementRef }) openFileRef!: ElementRef<any>;
  @ViewChild('closeFile', { read: ElementRef }) closefileRef!: ElementRef<any>;
  @ViewChild('openFolder', { read: ElementRef }) openFolderRef!: ElementRef<any>;
  @ViewChild('closeFolder', { read: ElementRef }) closeFolderRef!: ElementRef<any>;
  @ViewChild('openinputparam', { read: ElementRef }) openParamsRef!: ElementRef<any>;
  @ViewChild('closeinputparam', { read: ElementRef }) closeParamsRef!: ElementRef<any>;
  @ViewChild('openoutputfield', { read: ElementRef }) openFieldsRef!: ElementRef<any>;
  @ViewChild('closeoutputfield', { read: ElementRef }) closeFieldsRef!: ElementRef<any>;
  @ViewChild('openOutputEditField', { read: ElementRef }) closeEditoutputfieldRef!: ElementRef<any>;
  @ViewChild('closeOutputEditField', { read: ElementRef }) modelEditoutputfieldRef!: ElementRef<any>;
  @ViewChild('staticTabs', { static: false }) staticTabs?: TabsetComponent;

  parameterModalRef!: BsModalRef;
  addEditReportBuilderModalRef!:BsModalRef;
  fieldModalRef!:BsModalRef;
  customFieldModalRef!:BsModalRef;



  datainputParameters$!: Observable<any>

  tabs: ITab[] = [
    { title: 'Servion Reports', content: 'Dynamic content 1', removable: false, disabled: false },
    { title: 'MOE Reports', content: 'Dynamic content 2', removable: false, disabled: false },
  ];

  subFoldername: string = "";
  // String type variables
  searchKey: string = '';
  directory:any;
  isEdit :any;
  showErrors=false;
  directoryFieldsValid=false;
  reportType=[{name:'',value:''}];

  checked: boolean = false;
  tabsOverviewShow: boolean = false;
  // List type variables
  formattedDate: any;
  dateString: string = "2024-03-10T 10:10:10"; // Note: Use 'T' instead of space for ISO 8601 compliance
  dateObject = new Date(this.dateString);
  borderprimaryActive: boolean = false;
  datasourceitems: any
  datasourceactiveItem: any | undefined;
  subfilename: string = '';
  currentStep = 1;
  numSteps = 4;
  configPage: boolean = false;
  basicDetailspage: boolean = false;
  spinnerTriggered: boolean = false;
  inputParameters: boolean = false;
  OutputFields: boolean = false;
  
  // Example data array
  dataSourceCards: DataSourceCard[] = [
    {
      DataSourceName: "Data 0",
      DataSoureType: "Primary",
      Databasename: "DB1",
      hostname: "localhost",
      dateString: "2023-04-01T14:30:00Z"
    },
    {
      DataSourceName: "Data 0",
      DataSoureType: "Secondary",
      Databasename: "DB2",
      hostname: "dbserver.example.com",
      dateString: "2023-04-02T09:45:00Z"
    },
    {
      DataSourceName: "Data 0",
      DataSoureType: "Primary",
      Databasename: "DB3",
      hostname: "database3.local",
      dateString: "2023-04-03T18:15:00Z"
    }
  ];

  // List type variables
  reportBuilderList: any[] = [];
  directoryList: any[] = [];
  collectionList: any[] = [];
  dataSourceList: any[] = [];

  index = 0;
  active = 0;
  items: MenuItem[] | undefined;

  countries: country[] | undefined;

  selectedCountry: country | null = null;

  constructor(
    private _toastr: ToastrService, private _apiService: ApiService,
    private _sharedService: SharedService, private router: Router,
    public messageService: MessageService, private renderer: Renderer2, private elRef: ElementRef,
    private _formBuilder: FormBuilder, private http: HttpClient,  private modalService: BsModalService
  ) { }

  firstFormGroup = this._formBuilder.group({
    firstCtrl: ['', Validators.required],
  });
  secondFormGroup = this._formBuilder.group({
    secondCtrl: ['', Validators.required],
  });

  nameAtoZ = "../../assets/images/nameAtoZ.png";
  nameZtoA = "../../assets/images/nameZtoA.png";
  DateOtoN = "../../assets/images/DateOtoN.png";
  DateNtoO = "../../assets/images/DateNtoO.png";
  categories: any[] = [
    {
      name: 'Name (A to Z)',
      key: 'A',
      img: "../../assets/images/nameAtoZ.png"
    },
    {
      name: 'Name (Z to A)',
      key: 'Z',
      img: "../../assets/images/nameZtoA.png"
    },
    {
      name: 'Date (Oldest to Newest)',
      key: 'O',
      img: "../../assets/images/DateOtoN.png"
    },
    {
      name: 'Date (Newest to Oldest)',
      key: 'N',
      img: "../../assets/images/DateNtoO.png"
    }
  ];

  foldername = [{
    name: 'MOE',
    disabled: true,
    removable: false,
  }, {
    name: 'Agent',
    disabled: true,
    removable: false,
  }];

  filename = [{
    name: 'Servion Reports',
    disabled: true,
    removable: false,
  },
  {
    name: 'Data Reports',
    disabled: true,
    removable: false,
  },
  ]

  directoryModel={
    "action": "addDirectory",
    "id": 0,
    "name": "",
    "type": "reportbuilder",
    "description": "",
    "parentid" : 0,
    "rootdir": "",
    "status": "active",
    "userid": 1
  }
  



  ngOnInit() {
    this.reportType[0]={
      name: this._sharedService.currentDirectoryPath.split('/')[4],
      value: this._sharedService.currentDirectoryPath.split('/')[4] }
    this.clear();
  };

  getAllReportBuilder() {
    //Get All Report Builders
    this._apiService
      .makeAPICall({
        action: 'getAllReportBuilder',
        searchkey: this.searchKey,
        parentid : this._sharedService.currentDirectoryParent,
        parentpath : this._sharedService.currentDirectoryPath
      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response?.data?.length > 0) {
            this.reportBuilderList = response.data;
          
          } else {
          }
          console.log('getAllReportBuilder response', response);
        }
      });
  }

  getScreenDirectories() {
    //Get Report Builder Directories
    this._apiService
      .makeAPICall({
        action: 'getScreenDirectories',
        searchkey: this.searchKey,
        parentid : this._sharedService.currentDirectoryParent,
        rootdir: this._sharedService.currentDirectoryPath
      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response?.data?.length > 0) {
            this.directoryList = response.data;

           
          } else {
          }
          console.log('getscreendirectories response', response);
        }
      });
  }



  // Method to Load all necessary data list from Database.
  LoadAllData = () => {

    //Get Data Sources
    this._apiService
      .makeAPICall({
        action: 'getAllDataSourceIdName',
        searchKey: this.searchKey,
      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response.data.length > 0) {
            this.directoryList = response.data;
            this._toastr.success('Data Fetched Successfully');
          } else {
            this._toastr.warning('No Data Found');
          }
        }
      });

    //Get Collections
    this._apiService
      .makeAPICall({
        action: 'getAllCollectionIdName',
        searchKey: this.searchKey,
      }).subscribe((response) => {
        if (response && response.status) {
          if (response.data.length > 0) {
            this.directoryList = response.data;
            this._toastr.success('Data Fetched Successfully');
          } else {
            this._toastr.warning('No Data Found');
          }
        }
      });
  };


  onRadioChange(key: any) {

  }

  addNewTab(): void {
    const newTabIndex = this.tabs.length + 1;
    this.tabs.push({
      title: `Dynamic Title ${newTabIndex}`,
      content: `Dynamic content ${newTabIndex}`,
      disabled: false,
      removable: true
    });
  }

  removeTabHandler(tab: ITab): void {
    this.tabs.splice(this.tabs.indexOf(tab), 1);
    console.log('Remove Tab handler');
  }

  @ViewChild('stepper', { read: ViewContainerRef }) stepperContainer: ViewContainerRef | undefined;

  nextStep(): void {
    this.currentStep++;
    console.log("current step is:", this.currentStep);
    if (this.currentStep > this.numSteps) {
      this.currentStep = 1;
    }

   
    const steps = document.querySelectorAll('.step');

    steps.forEach((step: any, index: any) => {
      const stepNum = index + 1;
      if (stepNum === this.currentStep) {
        this.renderer.addClass(step, 'editing');
      } else {
        this.renderer.removeClass(step, 'editing');
      }
      if (stepNum < this.currentStep) {
        this.renderer.addClass(step, 'done');
      } else {
        this.renderer.removeClass(step, 'done');
      }
    });

    if (this.currentStep === 2) {
      this.currentStepperPage='config';
    } else if (this.currentStep === 3) {
      this.currentStepperPage='params';
    } else if (this.currentStep === 4) {
      this.currentStepperPage='fields';
    } else {
      this.currentStepperPage='basic';
    }
  }

  backStep(step: any) {
    if (this.currentStep > 1) {
      this.currentStep--;
    } else {
      this.currentStep = this.numSteps;
    }
    const steps = document.querySelectorAll('.step');

    steps.forEach((step: any, index: any) => {
      const stepNum = index + 1;
      if (stepNum === this.currentStep) {
        this.renderer.addClass(step, 'editing');
      } else {
        this.renderer.removeClass(step, 'editing');
      }
      if (stepNum < this.currentStep) {
        this.renderer.addClass(step, 'done');
      } else {
        this.renderer.removeClass(step, 'done');
      }
    });
    if (this.currentStep === 2) {
      this.currentStepperPage='config';
    } else if (this.currentStep === 3) {
      this.currentStepperPage='params';
    } else if (this.currentStep === 4) {
      this.currentStepperPage='fields';
    } else {
      this.currentStepperPage='basic';
    }

  }

  // Method to reset/clear all fields.
  clear = () => {
    this.dataSourceList = [];
    this.reportBuilderList = [];
    this.directoryList = [];
    this.collectionList = [];
    this.searchKey = '';
    this.directory= Object.assign({},this.directoryModel);
  

    this.getScreenDirectories();
    this.getAllReportBuilder();
  };

  // Method to call when search icon is pressed.
  search = () => {
    this.getAllDatasource();
  };

  // On change in sorting petion.
  onSortChange = (field: string, order: string) => {
    this.dataSourceList = this._sharedService.sorting(
      this.dataSourceList,
      field,
      order
    );
  };

  // Method to Get All Datasource from Database.
  getAllDatasource = () => {
    this._apiService
      .makeAPICall({
        action: 'getAllDataSource',
        searchKey: this.searchKey,
      })
      .subscribe((response: any) => {
        if (response && response.status) {
          if (response.data.length > 0) {
            this.dataSourceList = response.data;
            this._toastr.success('Data Fetched Successfully');
          } else {
            this._toastr.warning('No Data Found');
          }
        }
      });
  };

  


  private activeTabIndex = -1;

  onTabSelected(event: any) {
    this.activeTabIndex = event.index;
  }

  isActive(index: number): boolean {
    this.borderprimaryActive = true;
    this.activeTabIndex === index;
    return true;
  }

  getStepperClasses(index: number): object {
    return {
      'bg-primary border-primary': index <= this.active,
      'surface-border': index > this.active
    };
  }

  //Directory Methods
  openAddEditFolderModal() {
    this.resetAddEditFolderPopup();
    if(this.isEdit){
    }
    else{
      this.directory = this.directory = Object.assign({}, this.directoryModel);
    }
    //this.basicDetailspage = true;
  }

  closeFolderModal() {
    this.closeFolderRef.nativeElement.click();
    const element = this.elRef.nativeElement.querySelector(`modal-backdrop.show`);
    if (element) {
      this.renderer.removeClass(element, 'modal-backdrop.show');
    }
  }

  resetAddEditFolderPopup(){
   this.showErrors=false;
   this.directoryFieldsValid=false;
  }

  directoryFieldValidation(){
   if( this.directory.name!=''){
    this.directoryFieldsValid=true;
    return;
   }
  }

  mapEditDirectory(data:any){
    this.directory=Object.assign({},this.directoryModel);
    this.directory={
    action: 'editDirectory',
    id: data.id,
    name: data.name,
    type: data.type,
    description: data.description,
    parentid: data.parentid,
    rootdir: data.rootdir,
    status: data.status,
    userid: 1
    }
  }

  storeDirectory(): void {

    this.showErrors=true;
    this.directoryFieldValidation();
    
    if(!this.directoryFieldsValid){
      this._toastr.info('Please Fill the required fields');
        return;
    }

    if(this.isEdit){

    }
    else{
    this.directory.parentid=this._sharedService.currentDirectoryParent;
    this.directory.rootdir=this._sharedService.currentDirectoryPath;
    }

    console.log('store data',this.directory);


    //ADD - if directory fields are validated , Store it
    //EDIT -if directory fields are validated, save it
    this._apiService
      .makeAPICall(this.directory)
      .subscribe((response) => {

        if (response.status && response.data.message.split('-')[1] === 'true') {
          this._toastr.success(('Folder ' + (this.isEdit ? 'Edited' : 'Added' + ' successfully')));
          this.clear();
          this.closeFolderRef.nativeElement.click();
        }
        else {
          this._toastr.error((this.isEdit ? 'Update Error' : 'Insert Error'), response.data.message.split('-')[0], { disableTimeOut: true });
        }
        console.log('response', response);
      });
  }

  toggleDeleteModal(action: string, template: TemplateRef<any>) {
    if (action === 'open')
      this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false,
      });
    else if (action === 'close')
      this.modalService.hide();
  }
  
  // Method to Delete directory from Database.
  deleteDirectory (){
    this.directory.action='deleteDirectory';
    this._apiService
    .makeAPICall(this.directory)
    .subscribe((response) => {

      if (response.status && response.data.message.split('-')[1] === 'true') {
        this._toastr.success('Folder deleted successfully');
        this.clear();
        this.modalService.hide();
      }
      else {
        this._toastr.error('Delete error', response.data.message.split('-')[0], { disableTimeOut: true });
      }
      console.log('response', response);
    });
   };



   //ADD and Edit Report Builder File 

   currentStepperPage='';
   reportbuilderModel={};
   reportbuilder=this.reportbuilderModel;

  resetAddEditPopup(){
    this.showErrors=false;
    this.currentStepperPage='';
    this.currentStep = 2;
    this.backStep(1);
  }

  addEditReportBuilderPopup(action: string, template: TemplateRef<any>) {

    if (action === 'open') {
      this.resetAddEditPopup();
      this.addEditReportBuilderModalRef = this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false,
      });

      

      if (this.isEdit) {
        this.currentStep = 1;
        const steps = this.elRef.nativeElement.querySelectorAll('.step');

        steps.forEach((step: any, index: any) => {
          if (index === 0) {
            this.renderer.addClass(step, 'editing');
            this.renderer.removeClass(step, 'done');
          } else {
            this.renderer.addClass(step, 'done');
            this.renderer.removeClass(step, 'editing');
          }
        });

        console.log('Edit Report builder', this.reportbuilder);

      }
      else {
        this.reportbuilder = Object.assign({}, this.reportbuilderModel);
        console.log('Add Report builder', this.reportbuilder);
      }
    } else{
      this.addEditReportBuilderModalRef?.hide()
    }
  }

  closeFileModal() {
    this.closefileRef.nativeElement.click();
    this.basicDetailspage = false;
    this.inputParameters = false;
    this.configPage = false;
    this.inputParameters = false;
    const element = this.elRef.nativeElement.querySelector(`modal-backdrop.show`);
    if (element) {
      this.renderer.removeClass(element, 'modal-backdrop.show');
    }
  }
  
  parameterPopup(action: string,template: TemplateRef<any>) {
    if(action==='open'){
      this.parameterModalRef = this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false
      });
    } else{
      this.parameterModalRef.hide();
    }
   
  }

  fieldPopup(action: string,template: TemplateRef<any>) {
    if(action==='open'){
      this.fieldModalRef = this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false
      });
    } else{
      this.fieldModalRef.hide();
    }
   
  }

  customFieldPopup(action: string,template: TemplateRef<any>) {
    if(action==='open'){
      this.customFieldModalRef = this.modalService.show(template, {
        backdrop: 'static',
        keyboard: false
      });
    } else{
      this.customFieldModalRef.hide();
    }
   
  }

  closeInputParameterModel() {
    this.closeParamsRef.nativeElement.click();
    // const element = this.elRef.nativeElement.querySelector(`modal-backdrop.show`);
    // if (element) {
    //   this.renderer.removeClass(element, 'modal-backdrop.show');
    // }
  }

  openoutputModal() {
    this.openFieldsRef.nativeElement.click();
    this.basicDetailspage = false;
  }
  closeoutputModal() {
    this.closeFieldsRef.nativeElement.click();
    this.basicDetailspage = false;
    this.inputParameters = false;
    this.configPage = false;
    this.inputParameters = false;
    const element = this.elRef.nativeElement.querySelector(`modal-backdrop.show`);
    if (element) {
      this.renderer.removeClass(element, 'modal-backdrop.show');
    }
  }

  editoutputfieldsModel() {
    this.openFieldsRef.nativeElement.click();
    this.basicDetailspage = false;
  }
  closeEditoutputModal() {
    this.modelEditoutputfieldRef.nativeElement.click();
    this.basicDetailspage = false;
    this.inputParameters = false;
    this.configPage = false;
    this.inputParameters = false;
    const element = this.elRef.nativeElement.querySelector(`modal-backdrop.show`);
    if (element) {
      this.renderer.removeClass(element, 'modal-backdrop.show');
    }
  }

  saveeditoutputfieldparam() {
    this.basicDetailspage = false;
    this.inputParameters = false;
    this.configPage = false;
    this.inputParameters = false;
    const element = this.elRef.nativeElement.querySelector(`modal-backdrop.show`);
    if (element) {
      this.renderer.removeClass(element, 'modal-backdrop.show');
    }
  }
  validation() {
    console.log("output field validation")
  }

  createnewFile() {
    const newTabIndex = this.filename.length + 1;
    this.filename.push({
      name: this.subfilename,
      disabled: false,
      removable: true,
    });
    this.closeFileModal();
  }

  saveoutputfieldparam() {
    console.log("save output field");
    this.basicDetailspage = false;
    this.inputParameters = false;
    this.configPage = false;
    this.inputParameters = false;
    const element = this.elRef.nativeElement.querySelector(`modal-backdrop.show`);
    if (element) {
      this.renderer.removeClass(element, 'modal-backdrop.show');
    }
  }


  captureactive(value: any) {
    console.log("what is the value:", value)
  }

  TestConnection() {
    this.spinnerTriggered = true;
    setTimeout(() => {
      this.spinnerTriggered = false;

    }, 3000)
  }

  //REaltime,Historical Route navigation

  filemanagement(folder: any) {
    this._sharedService.currentDirectoryParent=folder.id;
    this._sharedService.currentDirectoryPath+=(folder.name+'/');
    this.router.navigate(['/Report-Builder', folder.name, 'Report-Builder-Folder']);
    this.clear();
  }

  folderfilemanagement(filename: any) {
    this.tabsOverviewShow = true;
    if (filename) {
      setTimeout(() => {
        this.selectTab(1)
      }, 500)
    }
  }

  selectTab(tabId: number) {
    if (this.staticTabs?.tabs[tabId]) {
      this.staticTabs.tabs[tabId].active = true;
    }
  }

  saveEditparam() {

  }
}

