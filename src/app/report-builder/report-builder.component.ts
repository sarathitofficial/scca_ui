// Default import
import { Component } from '@angular/core';

// Component related service import
import { ApiService } from '../services/api.service';

// Third party service import
import { ToastrService } from 'ngx-toastr';

// Shared service import
import { SharedService } from '../services/shared.service';
import { Data, Router } from '@angular/router';

@Component({
  selector: 'app-report-builder',
  templateUrl: './report-builder.component.html',
  styleUrls: ['./report-builder.component.scss'],
})
export class ReportBuilderComponent {
  // String type variables
  searchKey: string = '';

  // List type variables
  reportBuilderList: any[] = [];
  directoryList: any[] = [];
  collectionList: any[] = [];
  dataSourceList: any[] = [];

  foldername = [{
    name: 'Standard',
  },{
    name:'Custom'
  }];
  

  constructor(
    private _apiService: ApiService,
    private _toastr: ToastrService,
    private _sharedService: SharedService,private router: Router
  ) {}

  ngOnInit () {
    this.clear();
  };

  
  filemanagement(foldername:any){
    this._sharedService.currentDirectoryPath+=('/'+foldername+'/');
    this.router.navigate(['/Report-Builder',foldername])
      }

  // Method to reset/clear all fields.
  clear = () => {
    this._sharedService.currentDirectoryPath='reportbuilder';
    this._sharedService.currentDirectoryParent=-1;
  };

  // Method to call when search icon is pressed.
  search = () => {
    this.LoadAllData();
  };

  // On change in sorting option.
  onSortChange = (field: string, order: string) => {
    this.reportBuilderList = this._sharedService.sorting(
      this.reportBuilderList,
      field,
      order
    );
  };

  // Method to Load all necessary data list from Database.
  LoadAllData = () => {
    //Get All Report Builders
    this._apiService
      .makeAPICall({
        action: 'getAllReportBuilder',
        searchKey: this.searchKey,
      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response.data.length > 0) {
            this.reportBuilderList = response.data;
            this._toastr.success('Data Fetched Successfully');
          } else {
            this._toastr.warning('No Data Found');
          }
        }
      });

    //Get Report Builder Directories
    this._apiService
      .makeAPICall({
        action: 'getScreenDirectories',
        searchKey: this.searchKey,
      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response.data.length > 0) {
            this.directoryList = response.data;
            this._toastr.success('Data Fetched Successfully');
          } else {
            this._toastr.warning('No Data Found');
          }
        }
      });

    //Get Data Sources
    this._apiService
      .makeAPICall({
        action: 'getAllDataSourceIdName',
        searchKey: this.searchKey,
      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response.data.length > 0) {
            this.directoryList = response.data;
            this._toastr.success('Data Fetched Successfully');
          } else {
            this._toastr.warning('No Data Found');
          }
        }
      });

    //Get Collections
    this._apiService
      .makeAPICall({
        action: 'getAllCollectionIdName',
        searchKey: this.searchKey,
      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response.data.length > 0) {
            this.directoryList = response.data;
            this._toastr.success('Data Fetched Successfully');
          } else {
            this._toastr.warning('No Data Found');
          }
        }
      });
  };

  // Insert Report builder if type is 'add' else updates the existing report builder.
  storeReportBuilder = (type: string) => {
    let action = type === 'add' ? 'addReportBuilder' : 'editReportBuilder';
    this._apiService
      .makeAPICall({ action: action })
      .subscribe((response) => {});
  };

  // Method to Delete Datasource from Database.
  deleteReportBuilder = () => {
    let obj = {};
    this._apiService.makeAPICall(obj).subscribe((response) => {});
  };
}
