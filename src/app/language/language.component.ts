import { Component } from '@angular/core';

@Component({
  selector: 'app-language',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.scss'],
})
export class LanguageComponent {
  countries: any[] | undefined;

  SelectedLanguage: any | undefined;

  ngOnInit() {
    this.countries = [
      { name: 'English', code: 'Eng' },
      { name: 'Spanish', code: 'Spa' },
    ];
  }
}
