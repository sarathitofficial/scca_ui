// Default import
import { Component } from '@angular/core';

// Component related service import
import { ApiService } from '../services/api.service';

// Third party service import
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-sso',
  templateUrl: './sso.component.html',
  styleUrls: ['./sso.component.scss'],
})
export class SsoComponent {
  //string variables

  identityProvider: string = '';
  clientId: string = '';
  clientSecret: string = '';
  reirectUri: string = '';
  logoutUri: string = '';
  authentication: string = '';

  constructor(
    private _apiService: ApiService,
    private _toastr: ToastrService
  ) {}

  ngOnInit = () => {
    this.getSsoData();
  };

  // Method to reset/clear all fields.
  clear = () => {
    this.identityProvider = '';
    this.clientId = '';
    this.clientSecret = '';
    this.reirectUri = '';
    this.logoutUri = '';
    this.authentication = '';
  };

  // Method to get SSO data
  getSsoData = () => {
    this._apiService
      .makeAPICall({
        action: 'getSso',
      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response.result.length > 0) {
            this.identityProvider = response.result[0].identityProvider;
            this.clientId = response.result[0].clientId;
            this.clientSecret = response.result[0].clientSecret;
            this.reirectUri = response.result[0].reirectUri;
            this.logoutUri = response.result[0].logoutUri;
            this.authentication = response.result[0].authentication;

            this._toastr.success('Data Fetched Successfully');
          } else {
            this._toastr.warning('No Data Found');
          }
        }
      });
  };

  // Method to test SSO data
  testSsoData = () => {
    this._apiService
      .makeAPICall({
        action: 'testSso',
      })
      .subscribe((response) => {});
  };

  // Method to initial time add / update SSO data
  storeSsoData = () => {
    this._apiService
      .makeAPICall({
        action: 'storeSso',
      })
      .subscribe((response) => {});
  };
}
