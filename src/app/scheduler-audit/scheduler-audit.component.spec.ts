import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulerAuditComponent } from './scheduler-audit.component';

describe('SchedulerAuditComponent', () => {
  let component: SchedulerAuditComponent;
  let fixture: ComponentFixture<SchedulerAuditComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SchedulerAuditComponent]
    });
    fixture = TestBed.createComponent(SchedulerAuditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
