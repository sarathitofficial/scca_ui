import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.component.html',
  styleUrls: ['./folder.component.scss']
})
export class FolderComponent implements OnInit {
  foldername = [{
    name: 'Realtime',
  },{
    name:'Historical'
  }];
  


  constructor(private router:Router){}
  ngOnInit(): void {
    
  }

  filemanagement(foldername:any){
    this.router.navigate(['/Reports',foldername,'Reports-folder']);
  }
}
