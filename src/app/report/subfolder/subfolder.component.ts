import { Component,ElementRef,OnInit, Renderer2, ViewChild,OnDestroy, NgZone  } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';
import {  MessageService } from 'primeng/api';
import { MenuItem } from 'primeng/api';
import { StepsModule } from 'primeng/steps';
import { Observable, Subscription } from 'rxjs';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { HttpClient } from '@angular/common/http';
// Shared service import
import { SharedService } from 'src/app/services/shared.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DropdownFilterOptions } from 'primeng/dropdown';
import { ProductserviceService } from 'src/app/services/productservice.service';
import { DatepickerDateCustomClasses } from 'ngx-bootstrap/datepicker';

interface DataSourceCard {
  [x: string]: any;
    DataSourceName: string;
    DataSoureType: string;
    Databasename: string;
    hostname: string;
    dateString: string;
    // Add any other properties you need
  }

  interface country{
    name?:string;
    code?:string;
  }

  interface ITab {
    title: string;
    content: string;
    removable: boolean;
    disabled: boolean;
    active?: boolean;
    customClass?: string;
  }

  interface Product {
    id?: string;
    code?: string;
    name?: string;
    description?: string;
    price?: number;
    quantity?: number;
    inventoryStatus?: string;
    category?: string;
    image?: string;
    rating?: number;
}
interface Column {
  field: string;
  header: string;
}
@Component({
  selector: 'app-subfolder',
  templateUrl: './subfolder.component.html',
  styleUrls: ['./subfolder.component.scss']
})
export class SubfolderComponent implements OnInit{
  @ViewChild('openFile',{read: ElementRef})modelRef!:ElementRef<any>;
  @ViewChild('closeFile',{read: ElementRef})closeRef!:ElementRef<any>;
  @ViewChild('openFolder',{read: ElementRef})modelfolderRef!:ElementRef<any>;
  @ViewChild('closeFolder',{read: ElementRef})closefolderRef!:ElementRef<any>;
  @ViewChild('staticTabs', { static: false }) staticTabs?: TabsetComponent;

  dateCustomClasses!: DatepickerDateCustomClasses[];
  // String type variables
  searchKey: string = '';
  currentStep = 1;
  numSteps = 2;
  configPage:boolean = false;
  basicDetails:boolean = false;
  spinnerTriggered:boolean = false;
  tabsOverviewShow:boolean = false;
  ReportsTriggered:boolean = false;
  formattedDate:any;
      dateString:string = "2024-03-10T 10:10:10"; // Note: Use 'T' instead of space for ISO 8601 compliance
      dateObject = new Date(this.dateString);
  // List type variables
  collectionList: any[] = [];
  dataSourceList: any[] = [];
  activeIndex: number | undefined = 0;
  index = 0;
  active = 0;
  items: MenuItem[] | undefined;
  countries: country[] | undefined;
  sourceProducts!: any[];

  products!: Product[];

  cols!: Column[];

  _selectedColumns!: Column[];

  columnstable!:any;
  selectedColumnstable = [];

  targetProducts!: any[];
  tabs: ITab[] = [
    { title: 'Servion Reports', content: 'Dynamic content 1', removable: false, disabled: false},
    { title: 'MOE Reports', content: 'Dynamic content 2', removable: false, disabled: false},
  ];

  totalCount: Number = 0;  
  closeResult!: string;  
  dataParams: any = {  
    page_num: '',  
    page_size: ''  
  };  

  subFoldername:string ="";
selectedCountry: country | null = null;
foldername = [{
  name: 'MOE',
  disabled: true,
  removable: false,
},{
  name:'Agent',
  disabled: true,
  removable: false,
}];

filename:any=[{
  name: 'Servion Reports',
  disabled: true,
  removable: false,
},
{
  name: 'Data Reports',
  disabled: true,
  removable: false,
},
]
filterValue: string | undefined = '';

carService:any = {
  id: '1000',
  code: 'f230fh0g3',
  name: 'Bamboo Watch',
  description: 'Product Description',
  image: 'bamboo-watch.jpg',
  price: 65,
  category: 'Accessories',
  quantity: 24,
  inventoryStatus: 'INSTOCK',
  rating: 5
}

//table
cars = [
  { "brand": "VW", "year": 2012, "color": "Orange", "vin": "dsad231ff" },
  { "brand": "Audi", "year": 2011, "color": "Black", "vin": "gwregre345" },
  { "brand": "Renault", "year": 2005, "color": "Gray", "vin": "h354htr" },
  { "brand": "BMW", "year": 2003, "color": "Blue", "vin": "j6w54qgh" },
  { "brand": "Mercedes", "year": 1995, "color": "Orange", "vin": "hrtwy34" },
  { "brand": "Volvo", "year": 2005, "color": "Black", "vin": "jejtyj" },
  { "brand": "Honda", "year": 2012, "color": "Yellow", "vin": "g43gr" },
  { "brand": "Jaguar", "year": 2013, "color": "Orange", "vin": "greg34" },
  { "brand": "Ford", "year": 2000, "color": "Black", "vin": "h54hw5" },
  { "brand": "Fiat", "year": 2013, "color": "Red", "vin": "245t2s" }
] 


  constructor(
    private _apiService: ApiService, private productService:ProductserviceService,
    private _toastr: ToastrService,private renderer: Renderer2, private elRef: ElementRef,
    private _sharedService: SharedService,private cdr: ChangeDetectorRef, private router:Router,
  ) {
    const now = new Date();
    const twoDaysAhead = new Date();
    twoDaysAhead.setDate(now.getDate() + 2);
    const fourDaysAhead = new Date();
    fourDaysAhead.setDate(now.getDate() + 4);

    this.dateCustomClasses = [
      { date: now, classes: [] },
      { date: twoDaysAhead, classes: ['bg-warning'] },
      { date: fourDaysAhead, classes: ['bg-danger', 'text-warning'] }
    ];
  }

  ngOnInit = () => {
    this.clear();
    
        function formatDate(date: Date): string {
          const year = date.getFullYear();
          const month = (date.getMonth() + 1).toString().padStart(2, '0'); // Months are zero-based
          const day = date.getDate().toString().padStart(2, '0');
          const hours = date.getHours().toString().padStart(2, '0');
          const minutes = date.getMinutes().toString().padStart(2, '0');
          const seconds = date.getSeconds().toString().padStart(2, '0');
      
          return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
        
  };
  this.formattedDate = formatDate(this.dateObject);
  console.log(this.formattedDate);

  this.productService.getProductsMini().then((data) => {
    this.products = data;
});

this.cols = [
    { field: 'name', header: 'Name' },
    { field: 'category', header: 'Category' },
    { field: 'quantity', header: 'Quantity' }
];

this._selectedColumns = this.cols;

this.targetProducts = [];

}



  activeIndexChange(index : any){
    this.activeIndex = index
}

ngAfterViewInit() {
  this.cdr.detectChanges();
}
resetFunction(options: DropdownFilterOptions) {
  if(options){
    // options!.reset();
  }
  this.filterValue = '';
}

openRunModal(){
this.ReportsTriggered = true;
}
backpage(){
  this.ReportsTriggered = false;
 
}

customFilterFunction(event: KeyboardEvent, options: DropdownFilterOptions) {
  // options.filter(event);
}

openModal(){
  this.modelRef.nativeElement.click();
  this.basicDetails=true;
 }
 closeModal(){
  this.closeRef.nativeElement.click()
 }

 openFileModal(){
  this.modelRef.nativeElement.click();
  this.basicDetails=true;
  this.configPage=false;
 }
 closeFileModal(){
  this.closeRef.nativeElement.click();
  this.basicDetails=false;
  this.configPage=false;
 }
 openFolderModal(){
  this.modelfolderRef.nativeElement.click();
 }
 closeFolderModal(){
  this.closefolderRef.nativeElement.click();

 }

 addNewFolder(): void {
  const newTabIndex = this.foldername.length + 1;
  this.foldername.push({
    name: this.subFoldername,
    disabled: false,
    removable: true,
  });
  this.closeFolderModal();
}


 nextStep(): void {
  this.currentStep++;
  if (this.currentStep > this.numSteps) {
    this.currentStep = 1;
  }
  const steps = this.elRef.nativeElement.querySelectorAll('.step');

  steps.forEach((step:any, index:any) => {
    const stepNum = index + 1;
    if (stepNum === this.currentStep) {
      this.renderer.addClass(step, 'editing');
    } else {
      this.renderer.removeClass(step, 'editing');
    }
    if (stepNum < this.currentStep) {
      this.renderer.addClass(step, 'done');
    } else {
      this.renderer.removeClass(step, 'done');
    }
  });
 
    this.basicDetails=false;
    this.configPage = true;
 

}



folderfilemanagement(file:any){
  this.tabsOverviewShow = true;
  if(file){
    setTimeout(()=>{
      this.selectTab(1)
    },500)
  }

}

backStep(step: any){
  if (this.currentStep > 1) {
    this.currentStep--;
  } else {
    this.currentStep = this.numSteps;
  }
  const steps = this.elRef.nativeElement.querySelectorAll('.step');

  steps.forEach((step:any, index:any) => {
    const stepNum = index + 1;
    if (stepNum === this.currentStep) {
      this.renderer.addClass(step, 'editing');
    } else {
      this.renderer.removeClass(step, 'editing');
    }
    if (stepNum < this.currentStep) {
      this.renderer.addClass(step, 'done');
    } else {
      this.renderer.removeClass(step, 'done');
    }
  });

    this.basicDetails = true;
    this.configPage = false;

}

  // Method to reset/clear all fields.
  clear = () => {
    this.collectionList = [];
    this.searchKey = '';

    this.LoadAllData();
  };

  // Method to call when search icon is pressed.
  search = () => {
    this.LoadAllData();
  };

  // On change in sorting option.
  onSortChange = (field: string, order: string) => {
    this.collectionList = this._sharedService.sorting(
      this.collectionList,
      field,
      order
    );
  };

  // Method to Get All Collections from Database.
  LoadAllData = () => {
    //Get All Report Builders
    this._apiService
      .makeAPICall({
        action: 'getAllCollection',
        searchKey: this.searchKey,
      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response.data.length > 0) {
            this.collectionList = response.data;
            this._toastr.success('Data Fetched Successfully');
          } else {
            this._toastr.warning('No Data Found');
          }
        }
      });

    //Get collection
    this._apiService
      .makeAPICall({
        action: 'getAllDataSourceIdName',
        searchKey: this.searchKey,
      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response.data.length > 0) {
            this.dataSourceList = response.data;
            this._toastr.success('Data Fetched Successfully');
          } else {
            this._toastr.warning('No Data Found');
          }
        }
      });
  };

  // Insert collection if type is 'add' else updates the existing collection.
  storeReportBuilder = (type: string) => {
    let action = type === 'add' ? 'addCollection' : 'editCollection';
    this._apiService
      .makeAPICall({ action: action })
      .subscribe((response) => {});
  };

  // Method to Delete Collection from Database.
  deleteCollection = () => {
    let obj = {};
    this._apiService.makeAPICall(obj).subscribe((response) => {});
  };

  Validate(){
    this.spinnerTriggered=true;
    setTimeout(()=>{
      this.spinnerTriggered=false;

    },3000)
   }
   addNewTab(): void {
    const newTabIndex = this.tabs.length + 1;
    this.tabs.push({
      title: `Dynamic Title ${newTabIndex}`,
      content: `Dynamic content ${newTabIndex}`,
      disabled: false,
      removable: true
    });
  }
 
  removeTabHandler(tab: ITab): void {
    this.tabs.splice(this.tabs.indexOf(tab), 1);
    console.log('Remove Tab handler');
  }


   selectTab(tabId: number) {
    if (this.staticTabs?.tabs[tabId]) {
      this.staticTabs.tabs[tabId].active = true;
    }
  }

  get selectedColumns(): Column[] {
    return this._selectedColumns;
}

set selectedColumns(val: Column[]) {
    //restore original order
    this._selectedColumns = this.cols.filter((col) => val.includes(col));
}

}
