import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
  foldername = [{
    name: 'Standard',
  },{
    name:'Custom'
  }];
  


  constructor(private router:Router){}
  ngOnInit(): void {
    
  }

  filemanagement(foldername:any){
    this.router.navigate(['/Reports',foldername]);
  }
}
