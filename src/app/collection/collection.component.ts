import { Component,ElementRef,OnInit, Renderer2, ViewChild,OnDestroy, NgZone  } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import {  MessageService } from 'primeng/api';
import { MenuItem } from 'primeng/api';
import { StepsModule } from 'primeng/steps';
import { Subscription } from 'rxjs';
// Shared service import
import { TicketstepperService } from 'src/app/services/ticketstepper.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
// Component related service import
import { ApiService } from '../services/api.service';

// Third party service import
import { ToastrService } from 'ngx-toastr';

// Shared service import
import { SharedService } from '../services/shared.service';
interface country{
  name?:string;
  code?:string;
}

interface ITab {
  title: string;
  content: string;
  removable: boolean;
  disabled: boolean;
  active?: boolean;
  customClass?: string;
}


@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss'],
})
export class CollectionComponent {
  @ViewChild('open',{read: ElementRef})closeRef!:ElementRef<any>;
  @ViewChild('close',{read: ElementRef})modelRef!:ElementRef<any>;
  @ViewChild('openRecords',{read: ElementRef})closerecordsRef!:ElementRef<any>;
  @ViewChild('closeRecords',{read: ElementRef})modelrecordsRef!:ElementRef<any>;
  @ViewChild('staticTabs', { static: false }) staticTabs?: TabsetComponent;
  // String type variables
  searchKey: string = '';
  currentStep = 1;
  numSteps = 2;
  configPage:boolean = false;
  basicDetailspage:boolean = false;
  spinnerTriggered:boolean = false;
  tabsOverviewShow:boolean = false;
  formattedDate:any;
      dateString:string = "2024-03-10T 10:10:10"; // Note: Use 'T' instead of space for ISO 8601 compliance
      dateObject = new Date(this.dateString);
  // List type variables
  collectionList: any[] = [];
  dataSourceList: any[] = [];
  activeIndex: number | undefined = 0;
  index = 0;
  active = 0;
  items: MenuItem[] | undefined;
  countries: country[] | undefined;

  tabs: ITab[] = [
    { title: 'Servion Reports', content: 'Dynamic content 1', removable: false, disabled: false},
    { title: 'MOE Reports', content: 'Dynamic content 2', removable: false, disabled: false},
  ];

selectedCountry: country | null = null;
filename=[{
  name: 'Servion Reports',
  disabled: true,
  removable: false,
},
{
  name: 'Data Reports',
  disabled: true,
  removable: false,
},
]
  constructor(
    private _apiService: ApiService,
    private _toastr: ToastrService,private renderer: Renderer2, private elRef: ElementRef,
    private _sharedService: SharedService,private cdr: ChangeDetectorRef, private router:Router
  ) {}

  ngOnInit = () => {
    this.clear();
    
        function formatDate(date: Date): string {
          const year = date.getFullYear();
          const month = (date.getMonth() + 1).toString().padStart(2, '0'); // Months are zero-based
          const day = date.getDate().toString().padStart(2, '0');
          const hours = date.getHours().toString().padStart(2, '0');
          const minutes = date.getMinutes().toString().padStart(2, '0');
          const seconds = date.getSeconds().toString().padStart(2, '0');
      
          return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
        
  };
  this.formattedDate = formatDate(this.dateObject);
  console.log(this.formattedDate);
}
  activeIndexChange(index : any){
    this.activeIndex = index
}

ngAfterViewInit() {
  this.cdr.detectChanges();
}

openModal(){
  this.modelRef.nativeElement.click()
  this.basicDetailspage=true;
 }
 closeModal(){
  this.closeRef.nativeElement.click()
 }
 Recordsopenmodal(){
  this.closerecordsRef.nativeElement.click()
  this.basicDetailspage=false;
 }
 closeRecordsModal(){
  this.modelrecordsRef.nativeElement.click()
  this.basicDetailspage=false;
 }

 nextStep(): void {
  this.currentStep++;
  if (this.currentStep > this.numSteps) {
    this.currentStep = 1;
  }
  const steps = this.elRef.nativeElement.querySelectorAll('.step');

  steps.forEach((step:any, index:any) => {
    const stepNum = index + 1;
    if (stepNum === this.currentStep) {
      this.renderer.addClass(step, 'editing');
    } else {
      this.renderer.removeClass(step, 'editing');
    }
    if (stepNum < this.currentStep) {
      this.renderer.addClass(step, 'done');
    } else {
      this.renderer.removeClass(step, 'done');
    }
  });
 
    this.basicDetailspage=false;
    this.configPage = true;
 

}

folderfilemanagement(file:any){
  this.tabsOverviewShow = true;
  if(file){
    setTimeout(()=>{
      this.selectTab(1)
    },500)
  }

}

backStep(step: any){
  if (this.currentStep > 1) {
    this.currentStep--;
  } else {
    this.currentStep = this.numSteps;
  }
  const steps = this.elRef.nativeElement.querySelectorAll('.step');

  steps.forEach((step:any, index:any) => {
    const stepNum = index + 1;
    if (stepNum === this.currentStep) {
      this.renderer.addClass(step, 'editing');
    } else {
      this.renderer.removeClass(step, 'editing');
    }
    if (stepNum < this.currentStep) {
      this.renderer.addClass(step, 'done');
    } else {
      this.renderer.removeClass(step, 'done');
    }
  });

    this.basicDetailspage = true;
    this.configPage = false;

}

  // Method to reset/clear all fields.
  clear = () => {
    this.collectionList = [];
    this.searchKey = '';

    this.LoadAllData();
  };

  // Method to call when search icon is pressed.
  search = () => {
    this.LoadAllData();
  };

  // On change in sorting option.
  onSortChange = (field: string, order: string) => {
    this.collectionList = this._sharedService.sorting(
      this.collectionList,
      field,
      order
    );
  };

  // Method to Get All Collections from Database.
  LoadAllData = () => {
    //Get All Report Builders
    this._apiService
      .makeAPICall({
        action: 'getAllCollection',
        searchKey: this.searchKey,
      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response.data.length > 0) {
            this.collectionList = response.data;
            this._toastr.success('Data Fetched Successfully');
          } else {
            this._toastr.warning('No Data Found');
          }
        }
      });

    //Get collection
    this._apiService
      .makeAPICall({
        action: 'getAllDataSourceIdName',
        searchKey: this.searchKey,
      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response.data.length > 0) {
            this.dataSourceList = response.data;
            this._toastr.success('Data Fetched Successfully');
          } else {
            this._toastr.warning('No Data Found');
          }
        }
      });
  };

  // Insert collection if type is 'add' else updates the existing collection.
  storeReportBuilder = (type: string) => {
    let action = type === 'add' ? 'addCollection' : 'editCollection';
    this._apiService
      .makeAPICall({ action: action })
      .subscribe((response) => {});
  };

  // Method to Delete Collection from Database.
  deleteCollection = () => {
    let obj = {};
    this._apiService.makeAPICall(obj).subscribe((response) => {});
  };

  Validate(){
    this.spinnerTriggered=true;
    setTimeout(()=>{
      this.spinnerTriggered=false;

    },3000)
   }
   addNewTab(): void {
    const newTabIndex = this.tabs.length + 1;
    this.tabs.push({
      title: `Dynamic Title ${newTabIndex}`,
      content: `Dynamic content ${newTabIndex}`,
      disabled: false,
      removable: true
    });
  }
 
  removeTabHandler(tab: ITab): void {
    this.tabs.splice(this.tabs.indexOf(tab), 1);
    console.log('Remove Tab handler');
  }


   selectTab(tabId: number) {
    if (this.staticTabs?.tabs[tabId]) {
      this.staticTabs.tabs[tabId].active = true;
    }
  }
}
