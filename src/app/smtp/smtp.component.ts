// Default import
import { Component } from '@angular/core';

// Component related service import
import { ApiService } from '../services/api.service';

// Third party service import
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-smtp',
  templateUrl: './smtp.component.html',
  styleUrls: ['./smtp.component.scss'],
})
export class SmtpComponent {
  //string variables

  host: string = '';
  portNumber: string = '';
  security: string = '';
  username: string = '';
  password: string = '';
  senderEmail: string = '';
  replyToAddress: string = '';

  constructor(
    private _apiService: ApiService,
    private _toastr: ToastrService
  ) {}

  ngOnInit = () => {
    this.getSmtpData();
  };

  // Method to reset/clear all fields.
  clear = () => {
    this.host = '';
    this.portNumber = '';
    this.security = '';
    this.username = '';
    this.password = '';
    this.senderEmail = '';
    this.replyToAddress = '';
  };

  // Method to get SMTP data
  getSmtpData = () => {
    this._apiService
      .makeAPICall({
        action: 'getSmtp',
      })
      .subscribe((response) => {
        if (response && response.status) {
          if (response.result.length > 0) {
            this.host = response.result[0].host;
            this.portNumber = response.result[0].portNumber;
            this.security = response.result[0].security;
            this.username = response.result[0].username;
            this.password = response.result[0].password;
            this.senderEmail = response.result[0].senderEmail;
            this.replyToAddress = response.result[0].replyToAddress;

            this._toastr.success('Data Fetched Successfully');
          } else {
            this._toastr.warning('No Data Found');
          }
        }
      });
  };

  // Method to test SMTP data
  testSmtpData = () => {
    this._apiService
      .makeAPICall({
        action: 'testSmtp',
      })
      .subscribe((response) => {});
  };

  // Method to initial time add / update SMTP data
  storeSmtpData = () => {
    this._apiService
      .makeAPICall({
        action: 'storeSmtp',
      })
      .subscribe((response) => {});
  };
}
