import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthGuardService } from '../services/auth-guard.service';
import { SharedService } from '../services/shared.service';
import { AppComponent } from '../app.component';
import { FormControl, FormGroup} from '@angular/forms';
import { ApiService } from '../services/api.service';
import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  isLoggedIn='false';

  email:string='';
  password: string ='';
  setPassword:boolean=false;
  oldPassword: string='';
  newPassword: string ='';
  submitButtonValue = 'Log In';
  passwordset:boolean=false;
 
  constructor(private appComponent:AppComponent,private router: Router, private authService: AuthGuardService,private sharedService:SharedService,  private _apiService: ApiService, private _toastr : ToastrService ) {}

  
  login(submitvalue: any) {
   
     if (submitvalue == 'Log In'){

      this._apiService.makeAPICall
     ({
      action: 'signInUsers',
      username: this.email,
      password: this.password
     }).subscribe((response)=>
      {
        const message = JSON.parse(response.body).message;
        
        const token = JSON.parse(response.body).AuthenticationResult;
        
        const newpasswordRequired = JSON.parse(response.body).ChallengeName;
      
        
        if (token != undefined) {
          localStorage.setItem('Tokenid',token.AccessToken)
          this.appComponent.isLoggedIn=true;
          this.router.navigate(['/home']);
          this.isLoggedIn = 'true';
          this.sharedService.getloginDetails(this.isLoggedIn);
        } 
        else if(newpasswordRequired != undefined){
          this.setPassword=true;
          this.submitButtonValue = 'Set password';
          this._toastr.error(newpasswordRequired);
        }
        else {
          this._toastr.error(message);
        }
      });
     }
      if(this.submitButtonValue == 'Set password'){
        this._apiService.makeAPICall({
         action:"createUsers",
         type:"setPassword",
         username:this.email,
         password:this.newPassword
        }).subscribe((response)=>{
          console.log(response)
          // console.log(this.email,this.newPassword)
          this.setPassword=false;
          this.submitButtonValue = 'Log In';
         this._toastr.success("Password Changed Successfully");
         this.router.navigate(['/login']);
        })
     }
    
  }
}
