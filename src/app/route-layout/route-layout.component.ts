import { Component, OnInit } from '@angular/core';
import { SharedService } from '../services/shared.service';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Component({
  selector: 'app-route-layout',
  templateUrl: './route-layout.component.html',
  styleUrls: ['./route-layout.component.scss']
})
export class RouteLayoutComponent implements OnInit  {

  commonTitle!: string
  private titleChangeSubject = new BehaviorSubject<string>(this.commonTitle);
  
  constructor(public sharedService: SharedService){

  }

  
  ngOnInit(): void {
    this.commonTitle = this.sharedService.routeTitle;
    this.titleChangeSubject.subscribe(value => {
      console.log('commonTitle has changed:', value);
      var imgElement = document.getElementById("routeimage") as HTMLImageElement;
            imgElement.src = this.sharedService.routeImage;
    });
  }


}
